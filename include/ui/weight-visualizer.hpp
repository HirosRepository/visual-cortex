#ifndef WEIGHT_VISUALIZER_HPP
#define WEIGHT_VISUALIZER_HPP

#include "../settings.hpp"
#include "gui.hpp"
#include "shader.hpp"
#include "buffer.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <vector>

// ------------------------------------------------------------------------------------------------------------------ //

class WeightVisualizer
{
public:
    // Constructor -------------------------------------------------------------------------------------------------- //
    WeightVisualizer() {};
    
    // Initializer -------------------------------------------------------------------------------------------------- //
    void init(Gui &gui, const GLchar *vShaderPath, const GLchar *fShaderPath, GLuint marginSize = 20, GLfloat gapo = 10);
    void initVertices();
    
    // -------------------------------------------------------------------------------------------------------------- //
    
    // Draw all weights
    void draw(Array5d<float> &weights);
    
    // De-allocate All Loaded Resources
    void clear();
    
private:
    
    GLuint mMarginSize;
    GLfloat mGap;
    
    // Window shape
    GLuint mWidth;
    GLuint mHeight;
    
    // Top left corner location
    NLuint mRow;
    NLuint mCol;
    
    // Sizes
    NLuint mSize;             // size of the square on the screen in pixels
    NLuint mMapSize;          // number of neurons per row/column
    NLuint mReceptiveSize;    // size of receptive field size
    
    // Vertices for the square
    std::vector<float> mSquareVertices;
    
    // Projection Matrix
    glm::mat4 mProjection;
    
    Array1d<NLuint> mDataShape;
    
    // Shader
    Shader mSquareShader;
    
    // Buffer
    Buffer mSquareBuffer;
    
    // Heatmap
    glm::vec3 mHighColor;
    glm::vec3 mLowColor;
    
    // -------------------------------------------------------------------------------------------------------------- //
    
    // Draw a single square
    void drawSingle(NLuint mapRow, NLuint mapCol, NLuint recRow, NLuint recCol, float value);
    
    // Calculate the location of the square on the screen
    glm::vec2 calculateLocation(NLuint mapRow, NLuint mapCol, NLuint recRow, NLuint recCol, float &squareSize);
    
    // Calculate the projection matrix
    void calculateProjection(glm::vec2 location);
    
    glm::vec3 calculateColor(float value);
};


#endif /* WEIGHT_VISUALIZER_HPP */
