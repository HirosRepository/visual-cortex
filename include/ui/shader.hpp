#ifndef SHADER_HPP
#define SHADER_HPP

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <string>


// Shader Class ----------------------------------------------------------------------------------------------------- //
// This class compiles and manages GLSL shader programs
// ------------------------------------------------------------------------------------------------------------------ //

class Shader
{
public:
    // Constructors ------------------------------------------------------------------------------------------------- //
    Shader() {};
    
    // Getters ------------------------------------------------------------------------------------------------------ //
    GLuint getId();
    
    // Setters ------------------------------------------------------------------------------------------------------ //
    // TODO: Setters
    
    // Other Member Functions --------------------------------------------------------------------------------------- //
    
    // Load and Compile Shader from Source Code
    GLvoid loadShader(const GLchar *vShaderPath,
                      const GLchar *fShaderPath,
                      const GLchar *gShaderPath = nullptr);
    
    // Set Current Shader Active
    Shader &use(); // FIXME: Why this syntax?
    
    // Uniform Setters: sends values to uniform variables in shader ------------------------------------------------- //
    GLvoid setUniformInt    (const GLchar *name, GLint     value,       GLboolean useShader = false);
    GLvoid setUniformFloat  (const GLchar *name, GLfloat   value,       GLboolean useShader = false);
    GLvoid setUniformVec2   (const GLchar *name, GLfloat   x,
                             GLfloat   y,           GLboolean useShader = false);
    GLvoid setUniformVec2   (const GLchar *name, glm::vec2 &value,      GLboolean useShader = false);
    GLvoid setUniformVec3   (const GLchar *name, GLfloat   x,
                             GLfloat   y,
                             GLfloat   z,           GLboolean useShader = false);
    GLvoid setUniformVec3   (const GLchar *name, glm::vec3 &value,      GLboolean useShader = false);
    GLvoid setUniformVec4   (const GLchar *name, GLfloat   x,
                             GLfloat   y,
                             GLfloat   z,
                             GLfloat   w,           GLboolean useShader = false);
    GLvoid setUniformVec4   (const GLchar *name, glm::vec4 &value,      GLboolean useShader = false);
    GLvoid setUniformMat4   (const GLchar *name, glm::mat4 &value,      GLboolean useShader = false);
    
private:
    // State Variables ---------------------------------------------------------------------------------------------- //
    GLuint ID;
    
    // Member Functions --------------------------------------------------------------------------------------------- //
    
    // Compile Shader
    GLvoid compile(const GLchar *vertexSource,
                   const GLchar *fragmentSource,
                   const GLchar *geometrySource);
    
    // Check for Compile Errors
    GLvoid checkCompileErrors(GLuint object, std::string type);
    
};


#endif /* SHADER_HPP */
