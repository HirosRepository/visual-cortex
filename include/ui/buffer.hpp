#ifndef BUFFER_HPP
#define BUFFER_HPP

#include "../arrays.hpp"

#include <GL/glew.h>

#include <vector>

enum class BufferType
{
    STATIC,
    DYNAMIC,
};

// Buffer Class ----------------------------------------------------------------------------------------------------- //
// This class calculates the positions of vertices from the data received from the neural network and
// sends the results to the buffer
// ------------------------------------------------------------------------------------------------------------------ //

class Buffer
{
public:
    // Constructor -------------------------------------------------------------------------------------------------- //
    Buffer() {};
    
    // Getters ------------------------------------------------------------------------------------------------------ //
    GLuint getVAO();
    
    // Other Member Functions --------------------------------------------------------------------------------------- //
    void setupBuffer  (const std::vector<GLfloat> *vertices, GLenum usage, Array1d<GLuint> dataShape);
    
private:
    // State Variables ---------------------------------------------------------------------------------------------- //
    
    // Vertex Buffer Object
    GLuint mVBO;
    
    // Vertex Array Object
    GLuint mVAO;
};


#endif /* BUFFER_HPP */
