#ifndef ARRAYS_HPP
#define ARRAYS_HPP

#include "settings.hpp"

//FIXME: NLuint must check for minus for +=
//FIXME: check for different size
//FIXME: when double is entered for .fill it doesn’t work
//FIXME: better way to slice

// 1D array --------------------------------------------------------------------------------------------------------- //
template <class T>
class Array1d
{
public:
    // Constructors ------------------------------------------------------------------------------------------------- //
    Array1d() {}; //FIXME: do something about initialization
    Array1d(NLuint size);
    Array1d(NLuint size, T value);
    Array1d(Array1d<T> &array);
    Array1d(const Array1d<T> &array);
    
    // Destructor --------------------------------------------------------------------------------------------------- //
    ~Array1d();
    
    // Operators ---------------------------------------------------------------------------------------------------- //
    void operator= (Array1d<T> array);
    void operator= (T *array);
    
    T& operator[] (NLuint index);
    const T& operator[] (NLuint index) const;
    
    // Inplace single value operations
    void operator+= (NLuint value);
    void operator-= (NLuint value);
    void operator*= (NLuint value);
    void operator/= (NLuint value);
    
    void operator+= (int value);
    void operator-= (int value);
    void operator*= (int value);
    void operator/= (int value);
    
    void operator+= (NLfloat value);
    void operator-= (NLfloat value);
    void operator*= (NLfloat value);
    void operator/= (NLfloat value);
    
    // Inplace elementwise operations
    void operator+= (Array1d<NLuint> &array);
    void operator-= (Array1d<NLuint> &array);
    void operator*= (Array1d<NLuint> &array);
    void operator/= (Array1d<NLuint> &array);
    
    void operator+= (Array1d<NLfloat> &array);
    void operator-= (Array1d<NLfloat> &array);
    void operator*= (Array1d<NLfloat> &array);
    void operator/= (Array1d<NLfloat> &array);
    
    // -------------------------------------------------------------------------------------------------------------- //
    
    // Get size
    const NLuint size() const;
    
    // Allocate memory (only once)
    void allocate(NLuint size);
    
    // Fill by a single value
    void fill(NLuint  value);
    void fill(int   value);
    void fill(NLfloat value);
    void fill(bool  value);
    
    // Slice with first index and last index (inclusive)
    void slice(NLuint begin, NLuint end, Array1d<T> &array);
    
private:
    
    // Size
    NLuint mSize = 0;
    
    // Elements
    T *mElements = nullptr;
};



// 2D array --------------------------------------------------------------------------------------------------------- //
template <class T>
class Array2d
{
public:
    // Constructors ------------------------------------------------------------------------------------------------- //
    Array2d() { mShape.allocate(2); };
    Array2d(NLuint size0, NLuint size1);
    Array2d(NLuint size0, NLuint size1, T value);
    
    // Destructor --------------------------------------------------------------------------------------------------- //
    ~Array2d();
    
    // Operators ---------------------------------------------------------------------------------------------------- //
    void operator= (Array2d<T> &array2d);
    void operator= (T **array);
    
    T* operator[] (NLuint index);
    const T* operator[] (NLuint index) const;
    
    // Inplace single value operations
    void operator+= (NLuint value);
    void operator-= (NLuint value);
    void operator*= (NLuint value);
    void operator/= (NLuint value);
    
    void operator+= (int value);
    void operator-= (int value);
    void operator*= (int value);
    void operator/= (int value);
    
    void operator+= (NLfloat value);
    void operator-= (NLfloat value);
    void operator*= (NLfloat value);
    void operator/= (NLfloat value);
    
    // Inplace elementwise operations
    void operator+= (Array2d<NLuint> &array2d);
    void operator-= (Array2d<NLuint> &array2d);
    void operator*= (Array2d<NLuint> &array2d);
    void operator/= (Array2d<NLuint> &array2d);
    
    void operator+= (Array2d<NLfloat> &array2d);
    void operator-= (Array2d<NLfloat> &array2d);
    void operator*= (Array2d<NLfloat> &array2d);
    void operator/= (Array2d<NLfloat> &array2d);
    
    // -------------------------------------------------------------------------------------------------------------- //
    
    // Get size (total number of elements)
    const NLuint size() const;
    
    // Get shape
    const Array1d<NLuint> shape() const;
    
    // Allocate memory (only once)
    void allocate(Array1d<NLuint> shape);
    void allocate(NLuint size0, NLuint size1);
    
    // Fill by a single value
    void fill(NLuint  value);
    void fill(int   value);
    void fill(NLfloat value);
    void fill(bool  value);
    
    // Slice with first index and last index (inclusive)
    void slice(NLuint begin0, NLuint end0,
               NLuint begin1, NLuint end1,
               Array2d<T> &array2d);
    
private:
    
    // Size
    NLuint mSize;
    
    // Shape
    Array1d<NLuint> mShape;
    
    // Elements
    T **mElements = nullptr;
};


// 3D array --------------------------------------------------------------------------------------------------------- //
template <class T>
class Array3d
{
public:
    // Constructors ------------------------------------------------------------------------------------------------- //
    Array3d() { mShape.allocate(3); };
    Array3d(NLuint size0, NLuint size1, NLuint size2);
    Array3d(NLuint size0, NLuint size1, NLuint size2, T value);
    
    // Destructor --------------------------------------------------------------------------------------------------- //
    ~Array3d();
    
    // Operators ---------------------------------------------------------------------------------------------------- //
    void operator= (Array3d<T> &array3d);
    void operator= (T ***array3d);
    
    T** operator[] (NLuint index);
    const T** operator[] (NLuint index) const;
    
    // Inplace single value operations
    void operator+= (NLuint value);
    void operator-= (NLuint value);
    void operator*= (NLuint value);
    void operator/= (NLuint value);
    
    void operator+= (int value);
    void operator-= (int value);
    void operator*= (int value);
    void operator/= (int value);
    
    void operator+= (NLfloat value);
    void operator-= (NLfloat value);
    void operator*= (NLfloat value);
    void operator/= (NLfloat value);
    
    // Inplace elementwise operations
    void operator+= (Array3d<NLuint> &array3d);
    void operator-= (Array3d<NLuint> &array3d);
    void operator*= (Array3d<NLuint> &array3d);
    void operator/= (Array3d<NLuint> &array3d);
    
    void operator+= (Array3d<NLfloat> &array3d);
    void operator-= (Array3d<NLfloat> &array3d);
    void operator*= (Array3d<NLfloat> &array3d);
    void operator/= (Array3d<NLfloat> &array3d);
    
    // -------------------------------------------------------------------------------------------------------------- //
    
    // Get size (total number of elements)
    const NLuint size() const;
    
    // Get shape
    const Array1d<NLuint> shape() const;
    
    // Allocate memory (only once)
    void allocate(Array1d<NLuint> shape);
    void allocate(NLuint size0, NLuint size1, NLuint size2);
    
    // Fill by a single value
    void fill(NLuint  value);
    void fill(int   value);
    void fill(NLfloat value);
    void fill(bool  value);
    
    // Slice with first index and last index (inclusive)
    void slice(NLuint begin0, NLuint end0,
               NLuint begin1, NLuint end1,
               NLuint begin2, NLuint end2,
               Array3d<T> &array3d);
    
private:
    
    // Size
    NLuint mSize;
    
    // Shape
    Array1d<NLuint> mShape;
    
    // Elements
    T ***mElements = nullptr;
};

// 4D array --------------------------------------------------------------------------------------------------------- //
template <class T>
class Array4d
{
public:
    // Constructors ------------------------------------------------------------------------------------------------- //
    Array4d() { mShape.allocate(4); };
    Array4d(NLuint size0, NLuint size1, NLuint size2, NLuint size3);
    Array4d(NLuint size0, NLuint size1, NLuint size2, NLuint size3, T value);
    
    // Destructor --------------------------------------------------------------------------------------------------- //
    ~Array4d();
    
    // Operators ---------------------------------------------------------------------------------------------------- //
    void operator= (Array4d<T> &array4d);
    //void operator= (T ****array);
    
    T*** operator[] (NLuint index);
    const T*** operator[] (NLuint index) const;
    
    // Inplace single value operations
    void operator+= (NLuint value);
    void operator-= (NLuint value);
    void operator*= (NLuint value);
    void operator/= (NLuint value);
    
    void operator+= (int value);
    void operator-= (int value);
    void operator*= (int value);
    void operator/= (int value);
    
    void operator+= (NLfloat value);
    void operator-= (NLfloat value);
    void operator*= (NLfloat value);
    void operator/= (NLfloat value);
    
    // Inplace elementwise operations
    void operator+= (Array4d<NLuint> &array4d);
    void operator-= (Array4d<NLuint> &array4d);
    void operator*= (Array4d<NLuint> &array4d);
    void operator/= (Array4d<NLuint> &array4d);
    
    void operator+= (Array4d<NLfloat> &array4d);
    void operator-= (Array4d<NLfloat> &array4d);
    void operator*= (Array4d<NLfloat> &array4d);
    void operator/= (Array4d<NLfloat> &array4d);
    
    // -------------------------------------------------------------------------------------------------------------- //
    
    // Get size (total number of elements)
    const NLuint size() const;
    
    // Get shape
    const Array1d<NLuint> shape() const;
    
    // Allocate memory (only once)
    void allocate(Array1d<T> shape);
    void allocate(NLuint size0, NLuint size1, NLuint size2, NLuint size3);
    
    // Fill by a single value
    void fill(NLuint  value);
    void fill(int   value);
    void fill(NLfloat value);
    void fill(bool  value);
    
    // Slice with first index and last index (inclusive)
    void slice(NLuint begin0, NLuint end0,
               NLuint begin1, NLuint end1,
               NLuint begin2, NLuint end2,
               NLuint begin3, NLuint end3,
               Array4d<T> &array4d);
    
private:
    
    // Size
    NLuint mSize;
    
    // Shape
    Array1d<NLuint> mShape;
    
    // Elements
    T ****mElements = nullptr;
};

// 5D array --------------------------------------------------------------------------------------------------------- //
template <class T>
class Array5d
{
public:
    // Constructors ------------------------------------------------------------------------------------------------- //
    Array5d() { mShape.allocate(5); };
    Array5d(NLuint size0, NLuint size1, NLuint size2, NLuint size3, NLuint size4);
    Array5d(NLuint size0, NLuint size1, NLuint size2, NLuint size3, NLuint size4, T value);
    
    // Destructor --------------------------------------------------------------------------------------------------- //
    ~Array5d();
    
    // Operators ---------------------------------------------------------------------------------------------------- //
    void operator= (Array5d<T> &array5d);
    
    T**** operator[] (NLuint index);
    const T**** operator[] (NLuint index) const;
    
    // Inplace single value operations
    void operator+= (NLuint value);
    void operator-= (NLuint value);
    void operator*= (NLuint value);
    void operator/= (NLuint value);
    
    void operator+= (int value);
    void operator-= (int value);
    void operator*= (int value);
    void operator/= (int value);
    
    void operator+= (NLfloat value);
    void operator-= (NLfloat value);
    void operator*= (NLfloat value);
    void operator/= (NLfloat value);
    
    // Inplace elementwise operations
    void operator+= (Array5d<NLuint> &array5d);
    void operator-= (Array5d<NLuint> &array5d);
    void operator*= (Array5d<NLuint> &array5d);
    void operator/= (Array5d<NLuint> &array5d);
    
    void operator+= (Array5d<NLfloat> &array5d);
    void operator-= (Array5d<NLfloat> &array5d);
    void operator*= (Array5d<NLfloat> &array5d);
    void operator/= (Array5d<NLfloat> &array5d);
    
    // -------------------------------------------------------------------------------------------------------------- //
    
    // Get size (total number of elements)
    const NLuint size() const;
    
    // Get shape
    const Array1d<NLuint> shape() const;
    
    // Allocate memory (only once)
    void allocate(Array1d<NLuint> shape);
    void allocate(NLuint size0, NLuint size1, NLuint size2, NLuint size3, NLuint size4);
    
    // Fill by a single value
    void fill(NLuint  value);
    void fill(int   value);
    void fill(NLfloat value);
    void fill(bool  value);
    
    // Slice with first index and last index (inclusive)
    void slice(NLuint begin0, NLuint end0,
               NLuint begin1, NLuint end1,
               NLuint begin2, NLuint end2,
               NLuint begin3, NLuint end3,
               NLuint begin4, NLuint end4,
               Array5d<T> &array5d);
    
private:
    
    // Size
    NLuint mSize;
    
    // Shape
    Array1d<NLuint> mShape;
    
    // Elements
    T *****mElements = nullptr;
};


#endif /* ARRAYS_HPP */
