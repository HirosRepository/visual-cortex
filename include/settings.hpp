#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <GL/glew.h>

#include <string>

// typedefs
typedef unsigned int NLuint;
typedef int NLint;
typedef float NLfloat;


// Enum classes ---------------------------------------------------------------------------------------------------------- //
enum class SpikeGenerationMode
{
    SHARP,
    COARSE,
    COLOR,
};

enum class PatchSelectionMode
{
    STOCHASTIC,
    DETERMINISTIC,
};

enum class FiringMode
{
    DETERMINISTIC,
    STOCHASTIC,
};

enum class LateralMode
{
    SQUARE,
    ROUND,
};



const GLuint HEIGHT = 600;
const GLuint WIDTH  = 600;


// Visual input hyperparameters ------------------------------------------------------------------------------------------- //

const NLuint RECEPTIVE_SIZE = 7;
const NLuint INPUT_SIZE     = 200;
//const NLfloat PATCH_THRESHOLD = ;
const bool USE_SHARP_SPIKES = true;
const bool USE_COARSE_SPIKES = false;
const bool USE_COLOR_SPIKES = false;
const NLuint KERNEL_SIZE = 7;
const NLfloat SMALL_SIGMA = 1.0f;
const NLfloat LARGE_SIGMA = 2.0f;
//const NLuint DOG_THRESHOLD = ;
//const LoadMode =;
//const std::string FILE_PATH = ""
const NLuint NUM_FILES = 1;
const NLuint NUM_FRAMES = 10;


const NLuint UNIT_SIZE = 200;
const NLuint MAP_SIZE = 10;
//const NLuint MAIN_INPUTS = ??;


#endif /* SETTINGS_HPP */
