#ifndef RETINA_HPP
#define RETINA_HPP

#include "../image-loader.hpp"
#include "../arrays.hpp"

// Options ---------------------------------------------------------------------------------------------------------- //
enum class SpikeGenerationMode
{
    SHARP,
    COARSE,
    COLOR,
};

enum class PatchSelectionMode
{
    STOCHASTIC,
    DETERMINISTIC,
};

// Hyperparameters -------------------------------------------------------------------------------------------------- //

struct RetinaParameters
{
    ImageLoader *pImageLoader;
    NNuint patchSize;
    NNuint numPatches;
    NNuint numSpikes;
    NNuint filterSize;
    PatchSelectionMode mode;
    NNfloat threshold;
    NNfloat sigma1;
    NNfloat sigma2;
};

// ------------------------------------------------------------------------------------------------------------------ //


class Retina
{
public:
    // Constructor -------------------------------------------------------------------------------------------------- //
    Retina() {};
    
    // -------------------------------------------------------------------------------------------------------------- //
    
    void init(RetinaParameters params);
    
    // Returns indices of spikes
    const Array3d<NNuint>* generateSpikes(NNuint id, SpikeGenerationMode mode);
    
private:
    
    // Images
    ImageLoader *mImageLoader;          // pointer to the ImageLoader ojbect
    Array3d<NNfloat> *mImage;           // pointer to the current image
    Array2d<NNfloat> mDoGFilter;        // precomputed Difference of Gaussian
    
    // Image IDs
    NNuint mSharpId;                    // ID of the last sharp grayscale image added
    NNuint mCoarseId;                   // ID of the last coarse grayscale image added
    NNuint mColorId;                    // ID of the last color image added
    
    // Patches
    PatchSelectionMode mMode;           // either STOCHASTIC or DETERMINISTIC
    NNfloat mThreshold;                 // if activation is below this, discard the patch
    NNuint mPatchSize;                  // size of the patch from which spike will be produced
    NNuint mNumPatches;                 // number of patches per image
    
    // Spikes
    NNuint mNumSpikes;                  // number of spikes per patch
    
    // Remembered spikes: ID x patch locations x spike order x spike locations
    Array4d<NNuint> mSharpSpikes;       // spikes for sharp graycale images
    Array4d<NNuint> mCoarseSpikes;      // spikes for coarse grayscale images
    Array4d<NNuint> mColorSpikes;       // spikes for color images
    
    // -------------------------------------------------------------------------------------------------------------- //
    
    // Generate different types of spikes
    void generateSharpSpikes();
    void generateCoarseSpikes();
    void generateColorSpikes();
    
    // Patch processing
    void selectPatchesDeterministically(Array2d<NNfloat> &activation, Array2d<NNuint> &patchLocations);
    void selectPatchesStochastically(Array2d<NNfloat> &activation, Array2d<NNuint> &patchLocations);
    void addSpikes(Array2d<NNuint> &spikes, Array2d<NNfloat> &patch, Array1d<NNuint> patchLocation, NNuint id);
    
};

#endif /* RETINA_HPP */
