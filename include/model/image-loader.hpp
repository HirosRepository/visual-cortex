#ifndef IMAGE_LOADER_HPP
#define IMAGE_LOADER_HPP
#include "../../include/settings.hpp"
#include <opencv2/imgcodecs.hpp>
#include <string>
// ------------------------------------------------------------------------------------------------------------------ //

enum class LoadMode
{
    FROM_STREAM,
    FROM_VIDEO_FILE,
    FROM_IMAGE_FILES,
};

// ------------------------------------------------------------------------------------------------------------------ //

class ImageLoader
{
public:
    // Constructor -------------------------------------------------------------------------------------------------- //
    ImageLoader() {};
    
    // Initializer -------------------------------------------------------------------------------------------------- //
    void init(LoadMode mode);
    void init(LoadMode mode, std::string filePath);
    void init(LoadMode mode, std::string filePath, NLuint numFiles, NLuint numFrames);
    
    // Load single frame
    void loadImage(cv::Mat &frame);
    
private:
    
    // Loading mode
    LoadMode mMode;
    
    // File path
    std::string mFilePath;
    
    // Number of files (if from image files)
    NLuint mNumFiles;
    
    // FIXME: what’s this?
    NLuint mNumFrames;
    
    // FIXME: what’s this?
    NLuint mCurrentFile;
    NLuint mCurrentFrame;
    
    // Load image
    void loadFromStream(cv::Mat &frame);
    void loadFromVideoFile(cv::Mat &frame);
    void loadFromImageFiles(cv::Mat &frame);
};

#endif /* IMAGE_LOADER_HPP */
