#ifndef UNIT_HPP
#define UNIT_HPP

#include "../../include/arrays.hpp"
#include "../../include/model/layer.hpp"

#include <vector>

struct UnitParameters
{
    NLuint unitSize;
    NLuint mapSize;
    NLuint receptiveSize;
    NLuint mainInputs;
    NLuint numFrames;
    NLuint numSpikes; //per frame
    LayerParameters layerParams;
};


class Unit
{
public:
    Unit() {};
    // -------------------------------------------------------------------------------------------------------------- //
    void init(UnitParameters params, std::vector<Array1d<NLuint>> *inSpikes, Array3d<NLint> *inSpikeArray);
    
    void propagate();
    
//private:
    
    // Unit size
    NLuint mUnitSize;           // number of blocks x number of blocks
    
    // Number of frames kept
    NLuint mNumFrames;
    
    // Number of spikes sent per frame
    NLuint mNumSpikes;
    
    // Network shape
    NLuint mMapSize;            // size of the self-organizing map
    NLuint mReceptiveSize;      // size of the receptive field size
    NLuint mMainInputs;         // number of input channels
    NLuint mSubInputs;
    
    // Vector of pointers to incoming spikes
    std::vector<Array1d<NLuint>> *mInSpikes;
    std::vector<std::vector<Array1d<NLuint>>*> mSubInSpikes;
    
    // Vector pointers to arrays of spikes
    Array3d<NLint> *mInSpikeArray;
    std::vector<Array3d<NLint>*> mSubInSpikeArray;
    
    // Main layer
    Layer mMainLayer;
    
    // Main potentials
    Array4d<NLfloat> mMainPotentials;
    
    // Main past spikes
    Array5d<bool> mMainHasFired;
    
    // Outgoing spikes
    std::vector<Array1d<NLuint>> mMainOutSpikes;
    
    // -------------------------------------------------------------------------------------------------------------- //
    
    // Propagate a single spike
    void propagateSingleSpike(Array1d<NLuint> &spike);
    
    NLuint propagetToSingleBlock(Array1d<NLuint> &spike, bool ***hasFired);
    
    void propagateSublayer(Array1d<NLuint> &spike, Array1d<NLuint> &mainWinner);
    
    void retrieveMainSpike(Array1d<NLuint> &spike, std::vector<Array1d<NLuint>> &inSpike);
    
    void retrieveSubSpike(Array1d<NLuint> &spike, std::vector<Array1d<NLuint>> &inSpike);

    // Determine which blocks receive the spike
    void getRange(Array1d<NLuint> &spike, NLuint &startRow, NLuint &endRow, NLuint &startCol, NLuint &endCol);
    
    // Convert coordinate from that of unit to that of receptive field
    void convertCoordinate(Array1d<NLuint> &spike, Array1d<NLuint> &newSpike, NLuint blockRow, NLuint blockCol);
};

#endif /* UNIT_HPP */
