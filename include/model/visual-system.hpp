#ifndef VISUAL_SYSTEM_HPP
#define VISUAL_SYSTEM_HPP

#include "../settings.hpp"
#include "../image-loader.hpp"
#include "retina.hpp"
#include "v1.hpp"

#include <string>

// Hyperparameters -------------------------------------------------------------------------------------------------- //

struct VisualSystemParameters
{
    RetinaParameters retinaParams;
    V1Parameters     v1Params;
    bool             learningV1;
};

class VisualSystem
{
public:
    // Constructor -------------------------------------------------------------------------------------------------- //
    VisualSystem() {};
    
    // Initializer
    void init(ImageLoader *pImageLoader, VisualSystemParameters params);
    
    void setImagePaths(Array1d<std::string> &paths);
    
    void learn(NNuint epoch);
    
private:
    
    // Pointer to ImageLoader object
    ImageLoader *pImageLoader;
    
    // Retina
    Retina mRetina;
    
    // V1 cortex
    V1 mV1;
    
    // Image IDs
    NNuint mId;
    
    // Which layers to learn
    bool mLearningV1;
    
    // -------------------------------------------------------------------------------------------------------------- //
};

#endif /* VISUAL_SYSTEM_HPP */
