#ifndef LAYER_HPP
#define LAYER_HPP

#include "../arrays.hpp"

#include <vector>

// Hyperparameters for the Layer class ------------------------------------------------------------------------------ //
struct LayerParameters
{
    NLuint  mapSize;            // size of self-organizing map
    NLuint  receptiveSize;      // receptive field size
    NLuint  lateralSize;        // lateral connection size
    NLuint  numInputs;          // number of input channels
    NLfloat threshold;          // threshold between 0 and 1
    NLfloat aPlus;              // learning rate for potentiation
    NLfloat aMinus;             // learning rate for depression
    NLfloat mean;               // mean for Gaussian initializer
    NLfloat std;                // standard deviation initializer
};

// ------------------------------------------------------------------------------------------------------------------ //

class Layer
{
public:
    // Constructor -------------------------------------------------------------------------------------------------- //
    Layer() {};
    
    // Initializer -------------------------------------------------------------------------------------------------- //
    void init(LayerParameters params);
    
    // -------------------------------------------------------------------------------------------------------------- //
    
    // Propagate a single spike and learn it
    void propagate(NLfloat **potentials, Array1d<NLuint> &spike, bool ***hasFired, Array1d<NLuint> &winner);
    
//protected://FIXME: this needs changing
    
    // -------------------------------------------------------------------------------------------------------------- //
    
    // Network shape
    NLuint mMapSize;                // size of the self-organizing maps
    NLuint mReceptiveSize;          // size of the receptive field
    NLuint mLateralSize;            // distance between winner neuron and neighbors
    NLuint mNumInputs;              // number of input channels
    
    // Threshold for firing
    NLuint  mMaxPotential;          // total number of spikes that could arrive in one time step
    NLfloat mThreshold;             // between 0 and mMaxPotential
    
    // Learning rates
    NLfloat mAPlus;                 // learning rate for potentiation
    NLfloat mAMinus;                // learning rate for depression
    
    // Pointer to potentials
    NLfloat **mPotentials;          // points to the potential in Unit class
    
    // Record of spikes
    bool ***mHasFired;              // keep track of last spikes for learning
    
    // Weights
    Array5d<NLfloat> mWeights;      // mMapSize x mMapSize x mReceptiveSize x mReceptive Size x mNumInputs
    
    // Pointer to the vector of outgoing spikes
    std::vector<Array1d<NLuint>> *mOutSpikes;
    
    // Is winner found?
    bool mWinnerFound;
    
    //FIXME: should it have refractory period?
    
    // -------------------------------------------------------------------------------------------------------------- //
    
    // initializer -------------------------------------------------------------------------------------------------- //
    void initWeights(NLfloat mean, NLfloat std);
    
    // Propagation -------------------------------------------------------------------------------------------------- //
    
    // Increment and check if any neuron fired
    void searchCandidates(Array1d<NLuint> &spike, std::vector<Array1d<NLuint>> &candidates);
    
    // Check and add candidate to the vector
    void addCandidate(NLfloat potential, std::vector<Array1d<NLuint>> &candidates, NLuint row, NLuint col);
    
    // Add the winner spike to the vector of outgoing spikes
    void chooseWinner(std::vector<Array1d<NLuint>> &candidates, Array1d<NLuint> &winner);
    
    // Reset the state
    void reset();
    
    // Learning functions ------------------------------------------------------------------------------------------- //
    
    // Learn the pattern of the spikes
    void learn(Array1d<NLuint> &winner);
    
    // Get the correct range for lateral connection
    void getRange(NLuint &startRow, NLuint &endRow,
                  NLuint &startCol, NLuint &endCol,
                  NLuint neuronRow, NLuint neuronCol);
    
    // Update weights based on the past spike patterns
    void updateWeights(NLuint mapRow, NLuint mapCol);
};

#endif /* LAYER_HPP */
