#ifndef V1_HPP
#define V1_HPP

#include "primary-layer.hpp"

// Hyperparameters -------------------------------------------------------------------------------------------------- //

struct V1Parameters
{
    PrimaryLayerParameters primaryLayerParams;
};

class V1
{
public:
    // Constructor -------------------------------------------------------------------------------------------------- //
    V1() {};
    
    // Initializer
    void init(V1Parameters params);
    
    // -------------------------------------------------------------------------------------------------------------- //
    
    void learn(const Array3d<NNuint> *sharpSpikes);
    
private:
    
    PrimaryLayer mPrimaryLayer;
    
    // -------------------------------------------------------------------------------------------------------------- //
};

#endif /* V1_HPP */
