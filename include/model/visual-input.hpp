#ifndef VISUAL_INPUT_HPP
#define VISUAL_INPUT_HPP

#include "../arrays.hpp"
#include "unit.hpp"
#include "image-loader.hpp"

#include <opencv2/imgcodecs.hpp>

#include <deque>
#include <queue>
#include <string>
#include <map>

// ------------------------------------------------------------------------------------------------------------------ //
enum class ConvolutionMode
{
    ON_OFF,
    OFF_ON,
};

// ------------------------------------------------------------------------------------------------------------------ //

enum class ConvolutionSize
{
    SHARP,
    COARSE,
};

// ------------------------------------------------------------------------------------------------------------------ //
enum class SpikeType
{
    SHARP_GRAY,
    COARSE_GRAY,
    COLOR,//FIXME:remove
    RED_GREEN,
    BLUE_YELLOW,
};


// ------------------------------------------------------------------------------------------------------------------ //
struct VisualInputParameters
{
    NLuint      receptiveSize;      // size of the receptive field
    NLuint      inputSize;          // size of the side of the image
    NLuint      numKeptFrames;      // number of frames kept
    NLfloat     threshold;          // between 0 and 1, 0 means all patches are treated, 1 means none is
    bool        useSharpSpipkes;    // use sharp spikes
    bool        useCoarseSpikes;    // use coarse spikes
    bool        useColorSpikes;     // use color spikes
    NLuint      kernelSize;         // size of the kernel used for Gaussian blur
    NLfloat     smallSigma;         // smaller standard deviation for difference of Gaussian
    NLfloat     largeSigma;         // larger standard deviation for difference of Gaussian
    NLuint      DoGThreshold;       // between 0 and 255, no firing below this
    LoadMode    mode;               // FROM_STREAM, FROM_VIDEO_FILE, FROM_IMAGE_FILES
    std::string filePath;           // file path
    NLuint      numFiles;           // number of files, for loading image files
    NLuint      numFrames;          // FIXME: what’s this
};

// ------------------------------------------------------------------------------------------------------------------ //
class VisualInput
{
public:
    
    // Constructor -------------------------------------------------------------------------------------------------- //
    VisualInput() {};
    
    // Initializer -------------------------------------------------------------------------------------------------- //
    void init(VisualInputParameters params);
    
    // -------------------------------------------------------------------------------------------------------------- //
    
    // Load one frame per time step
    void loadFrame();
    
//private:
    
    // Input image size
    NLuint mInputSize;
    
    // Number of kept images
    NLuint mNumKeptFrames;
    
    // Receptive field size
    NLuint mReceptiveSize;
    
    // Threshold for number of spikes in the receptive field
    NLuint mThreshold;
    //NLuint mSharpThreshold;
    //NLuint mCoarseThreshold;
    //NLuint mColorThreshold;
    
    // ImageLoader object
    ImageLoader mImageLoader;
    
    // Which channels are active
    bool mUseSharpSpikes;
    bool mUseCoarseSpikes;
    bool mUseColorSpikes;
    
    // Has grayscale image already been generated?
    bool mGrayAvailable;
    
    // Parameters for the difference of Gaussian filter
    NLuint  mKernelSize;
    NLfloat mSmallSigma;
    NLfloat mLargeSigma;
    NLuint  mDoGThreshold;
    
    // Current frames
    cv::Mat mCurrentFrame;
    cv::Mat mCurrentGray;
    
    // Spikes generated
    std::map<SpikeType, std::vector<Array1d<NLuint>>> mSpikes;//FIXME: make this available
    //std::vector<Array1d<NLuint>> mSharpSpikes;
    //std::vector<Array1d<NLuint>> mColorSpikes;
    
    // Past spikes kept in memory
    Array3d<NLint> mSharpSpikeArray;
    Array3d<NLint> mRGSpikeArray;//FIXME: need to be in map
    Array3d<NLint> mBYSpikeArray;
    Array3d<NLint> mCoarseSpikeArray;//FIXME: make this accesible for others
    
    // Keep track which layer in SpikeArray represent current frame
    NLuint mCurrentFrameIdx;
    
    // Frame loading ------------------------------------------------------------------------------------------------ //
    
    // Generate spikes
    void generateGraySpikes (ConvolutionSize size);
    void generateColorSpikes();
    
    // Preprocessing
    void cropImage();
    void convertBGR2RGBY(cv::Mat &src, cv::Mat rg[], cv::Mat by[]);
    
    // Convolution by difference of Gaussian
    void convolveDoG     (cv::Mat &dst, ConvolutionMode mode, ConvolutionSize size);
    void convolveColorDoG(cv::Mat src[], cv::Mat &dst, ConvolutionMode mode);
    
    // Generate spikes from NLfloat values
    void convertToGraySpikes (cv::Mat &onOff, cv::Mat &offOn);
    void convertToColorSpikes(cv::Mat &RGOnOff, cv::Mat &RGOffOn, cv::Mat &BYOnOff, cv::Mat BYOffOn);
    
    // Check for threshold
    void checkSharpThreshold();
    void checkColorThreshold(Array3d<NLint> &spikeArray, SpikeType type);
    bool aboveThreshold(NLuint row, NLuint col, Array3d<NLint> &spikeArray);
};


#endif /* VISUAL_INPUT_HPP */
