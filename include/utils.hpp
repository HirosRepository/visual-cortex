#ifndef UTILS_HPP
#define UTILS_HPP

#include "settings.hpp"
#include "arrays.hpp"

#include <cmath>
#include <random>

// Image processing ------------------------------------------------------------------------------------------------- //

// 2D convolution
void convolve2d(Array2d<NNfloat> &src, Array2d<NNfloat> &dst, Array2d<NNfloat> filter)
{
    
}

NNfloat gaussian(NNfloat x, NNfloat sigma)
{
    NNfloat value = (1 / (2 * M_PI * sigma * sigma)) * std::exp(-x * x / (2 * sigma * sigma));
    return value;
}

NNfloat dog(NNfloat x, NNfloat sigma1, NNfloat sigma2)
{
    return gaussian(x, sigma1) - gaussian(x, sigma2);
}

Array2d<NNfloat> getDoGFilter(NNuint size, NNfloat sigma1, NNfloat sigma2)
{
    assert(size % 2 == 1);
    Array2d<NNfloat> filter(size, size);
    
    NNuint k = (size - 1) / 2;
    
    for (int row = 0; row < size; ++row)
    {
        for (int col = 0; col < size; ++col)
        {
            NNuint x = row - k;
            NNuint y = col - k;
            NNfloat distance = std::sqrt(x * x + y * y);
            filter[row][col] = dog(distance, sigma1, sigma2);
        }
    }
    
    filter = filter - filter.mean();
    filter = filter / filter.max();
    
    return filter;
}

// Convert to grayscale
void convertToGray(Array2d<NNfloat> &src, Array2d<NNfloat> &dst)
{
    
}

// Propagate spikes
void propagate()
{
    
}



// Random initializers ---------------------------------------------------------------------------------------------- //

// Generates value between 0 and 1 using Gaussian distribution
NNfloat generateGaussian(NNfloat mean, NNfloat std)
{
    std::normal_distribution<NNfloat> gaussian(mean, std);
    return gaussian(generator);
}

// Generates value between 0 and 1 using uniform distribution
NNfloat generateUiform()
{
    std::uniform_real_distribution<NNfloat> uniform(0.0f, 1.0f);
    return uniform(generator);
}

// Generates random index for an array
NNuint randomIndex(NNuint size)
{
    std::uniform_int_distribution<NNuint> uniform(0, size - 1);
    return uniform(generator);
}

// Miscellaneous ---------------------------------------------------------------------------------------------------- //

// Sorts by value and remembers the old indices in descending order
template <typename T>
void sortPairs(Array1d<T> &values, Array1d<NNuint> &indices, Array1d<std::pair<T, NNuint>> &values2indices)
{
    // Initialize indices
    NNuint size = values.size();
    for (int idx = 0; idx < size; ++idx)
        indices.emplace_back(idx);
    
    // Make a pair of values to old indices
    for (int idx = 0; idx < size; ++idx)
        values2indices.emplace_back(std::make_pair(values[idx], indices[idx]));
    
    // Sort by value
    std::sort(values2indices.begin(), values2indices.end(), std::greater<std::pair<NNfloat, NNuint>>());
}

#endif /* UTILS_HPP */
