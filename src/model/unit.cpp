#include "../../include/model/unit.hpp"

#include "../../include/utils.hpp"
#include <iostream>


// ------------------------------------------------------------------------------------------------------------------ //

// Initializer
void Unit::init(UnitParameters params,
                std::vector<Array1d<NLuint>> *inSpikes,
                Array3d<NLint> *inSpikeArray)
{
    // Unit size
    mUnitSize = params.unitSize;
    
    // Network shape
    mMapSize       = params.mapSize;
    mReceptiveSize = params.receptiveSize;
    mMainInputs    = params.mainInputs;
    mNumFrames     = params.numFrames;
    
    mNumSpikes = params.numSpikes;
    
    // Pointer to incoming spikes
    mInSpikes = inSpikes;
    
    // Pointer to incoming spike array
    mInSpikeArray = inSpikeArray;
    
    // Main layer
    mMainLayer.init(params.layerParams);
    
    // Allocate potentials
    mMainPotentials.allocate(mUnitSize, mUnitSize, mMapSize, mMapSize);
    mMainPotentials.fill(0.0f);
    
    // Allocate past spikes
    mMainHasFired.allocate(mUnitSize, mUnitSize, mReceptiveSize, mReceptiveSize, mMainInputs);
    mMainHasFired.fill(false);
}

// ------------------------------------------------------------------------------------------------------------------ //

// Propagate the whole unit and learn
void Unit::propagate()
{
    // clear spikes from the last time step
    mMainOutSpikes.clear();
    
    std::vector<NLuint> indices;
    NLuint vecSize = static_cast<NLuint>((*mInSpikes).size());
    for (NLuint idx = 0; idx < mNumSpikes; idx++)
        indices.emplace_back(randomIndex(vecSize));
        
    
    // Propagate all spikes
    for (auto idx : indices)
        propagateSingleSpike((*mInSpikes)[idx]);
}

// ------------------------------------------------------------------------------------------------------------------ //

// Propagate single spike and find outgoing spike
void Unit::propagateSingleSpike(Array1d<NLuint> &spike)
{
    // Out going spike
    Array1d<NLuint> outSpike(3);
    outSpike[0] = spike[0];
    outSpike[1] = spike[1];
    
    // Send the spikes and retrieve the type of the winner
    outSpike[2] = propagetToSingleBlock(spike, mMainHasFired[spike[0]][spike[1]]);
    
    //FIXME: add outgoing spikes
    mMainLayer.mWinnerFound = false;
}

// ------------------------------------------------------------------------------------------------------------------ //

// Send 3d slice of spike information and retrieve the winner type

NLuint Unit::propagetToSingleBlock(Array1d<NLuint> &spike, bool ***hasFired)
{
    std::vector<Array1d<NLuint>> inSpike;
    retrieveMainSpike(spike, inSpike);
    
    for (auto localSpike : inSpike)
    {
        Array1d<NLuint> mainWinner(3);
        
        mMainLayer.propagate(mMainPotentials[spike[0]][spike[1]], localSpike, hasFired, mainWinner);
        
        //propagateSublayer(spike, mainWinner, subWinner);
        
        if (mMainLayer.mWinnerFound)
        {
            NLuint winnerType = mainWinner[0] * mMapSize + mainWinner[1];
            return winnerType;
        }
    }
    return 0;
}



// ------------------------------------------------------------------------------------------------------------------ //
void Unit::propagateSublayer(Array1d<NLuint> &spike, Array1d<NLuint> &mainWinner)
{
    std::vector<Array1d<NLuint>> inSpike;
    
    for (auto localSpike : inSpike)
    {
        Array1d<NLuint> subWinner(3);
        //mSublayer.propagate(mSubPotentials[spike[0]][spike[1]], spikeFrames, mainWinner, subWinner);
    }
}

// ------------------------------------------------------------------------------------------------------------------ //
void Unit::retrieveMainSpike(Array1d<NLuint> &spike, std::vector<Array1d<NLuint>> &inSpike)
{
    NLuint radius = mReceptiveSize / 2;
    
    for (NLuint row = 0; row < mReceptiveSize; row++)
    {
        for (NLuint col = 0; col < mReceptiveSize; col++)
        {
            if ((*mInSpikeArray)[spike[0] - radius + row][spike[1] - radius + col][mNumFrames - 1] >= 0)
            {
                Array1d<NLuint> singleSpike(3);
                singleSpike[0] = row;
                singleSpike[1] = col;
                singleSpike[2] = (*mInSpikeArray)[spike[0] - radius + row][spike[1] - radius + col][mNumFrames - 1];
                inSpike.emplace_back(singleSpike);
            }
        }
    }
}

// ------------------------------------------------------------------------------------------------------------------ //
void Unit::retrieveSubSpike(Array1d<NLuint> &spike, std::vector<Array1d<NLuint>> &inSpike)
{
    NLuint radius = mReceptiveSize / 2;
    
    for (NLuint frame = 0; frame < mNumFrames; frame++)
    {
        for (NLuint row = 0; row < mReceptiveSize; row++)
        {
            for (NLuint col = 0; col < mReceptiveSize; col++)
            {
                for (NLuint channel = 0; channel < mSubInputs; channel++)
                {
                    if ((*mSubInSpikeArray[channel])[spike[0] - radius + row][spike[1] - radius + col][frame] >= 0)
                    {
                        NLuint featureType = (*mSubInSpikeArray[channel])[spike[0] - radius + row]
                                                                         [spike[1] - radius + col][frame];
                        Array1d<NLuint> singleSpike(4);
                        singleSpike[0] = row;
                        singleSpike[1] = col;
                        singleSpike[2] = channel * mSubInputs + featureType;
                        singleSpike[4] = frame;
                        inSpike.emplace_back(singleSpike);
                    }
                }
            }
        }
    }
}


// ------------------------------------------------------------------------------------------------------------------ //

// Determine which blocks receive the spike
void Unit::getRange(Array1d<NLuint> &spike, NLuint &startRow, NLuint &endRow, NLuint &startCol, NLuint &endCol)
{
    // Distance from the center of the receptive field
    NLuint radius = mReceptiveSize / 2;
    
    // If spike location is large enough, go back radius from center, otherwise radius from the top left corner
    startRow = (spike[0] >= radius * 2) ? spike[0] - radius : radius + 1;
    startCol = (spike[1] >= radius * 2) ? spike[1] - radius : radius + 1;
    
    // Distance to the bottom and right
    NLuint rowToEnd = mUnitSize - spike[0];
    NLuint colToEnd = mUnitSize - spike[1];
    
    // If distance to end is large enough, go forward radius from center, otherwise radius from end
    endRow = (rowToEnd > radius * 2) ? spike[0] + radius : mUnitSize - radius - 1;
    endCol = (colToEnd > radius * 2) ? spike[1] + radius : mUnitSize - radius - 1;
}
// ------------------------------------------------------------------------------------------------------------------ //

// Convert the coordinate of the spike in unit’s coordinate to receptive field’s coordinate
void Unit::convertCoordinate(Array1d<NLuint> &spike, Array1d<NLuint> &newSpike, NLuint blockRow, NLuint blockCol)
{
    // Distance from center of receptive field
    NLuint radius = mReceptiveSize / 2;
    
    newSpike[0] = spike[0] - (blockRow - radius);
    newSpike[1] = spike[1] - (blockCol - radius);
    newSpike[2] = spike[2];
}
// ------------------------------------------------------------------------------------------------------------------ //
