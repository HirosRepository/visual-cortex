#include "visual-system.hpp"

// Public functions ------------------------------------------------------------------------------------------------- //

void VisualSystem::init(ImageLoader *pImageLoader, VisualSystemParameters params)
{
    // Pointer to ImageLoader
    this->pImageLoader = pImageLoader;
    
    // Pass on the hyperparameters
    mRetina.init(params.retinaParams);
    mV1.init(params.v1Params);
    
    // Set learning layers
    mLearningV1 = params.learningV1;
}

// ------------------------------------------------------------------------------------------------------------------ //
// Sends image paths to the ImageLoader and assigns an ID
void VisualSystem::setImagePaths(Array1d<std::string> &paths)
{
    // Total number of images
    NNuint numImages = static_cast<NNuint>(paths.size());
    
    // Pass on the paths and set IDs
    for (int imageNo = 0; imageNo <= numImages; ++imageNo)
    {
        mId++; // because no image should have ID 0
        pImageLoader->setPath(mId, paths[imageNo]);
    }
}

// ------------------------------------------------------------------------------------------------------------------ //
void VisualSystem::learn(NNuint epoch)
{
    // Main learning loop
    for (int epochNo = 0; epochNo < epoch; ++epochNo)
    {
        // For all images
        for (int currentId = 1; currentId <= mId; ++currentId)
        {
            // Spikes from sharp grayscale images
            const Array3d<NNuint> *sharpSpikes = mRetina.generateSpikes(currentId, SpikeGenerationMode::SHARP);
            
            if (mLearningV1)
                mV1.learn(sharpSpikes);
        }
    }
}
// ------------------------------------------------------------------------------------------------------------------ //
