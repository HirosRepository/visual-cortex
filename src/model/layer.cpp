#include "../../include/model/layer.hpp"
#include "../../include/utils.hpp"

#include <iostream>

// ------------------------------------------------------------------------------------------------------------------ //

// Initializer
void Layer::init(LayerParameters params)
{
    // Network shape
    mMapSize       = params.mapSize;
    mReceptiveSize = params.receptiveSize;
    mLateralSize   = params.lateralSize;
    mNumInputs     = params.numInputs;
    
    // Maximum potential and threshold
    mMaxPotential = mReceptiveSize * mReceptiveSize * mNumInputs;
    mThreshold = static_cast<NLuint>(params.threshold * mMaxPotential);
    
    // Learning rates
    mAPlus  = params.aPlus;
    mAMinus = params.aMinus;
    
    // Initialize weights with Gaussian values
    initWeights(params.mean, params.std);
    
    // Winner not yet found
    mWinnerFound = false;
}

// ------------------------------------------------------------------------------------------------------------------ //

// Propagate and learn a single spike
void Layer::propagate(NLfloat **potentials, Array1d<NLuint> &spike, bool ***hasFired, Array1d<NLuint> &winner)
{
    // Get the pointer to the potentials
    mPotentials = potentials;
    
    // Get the pointer to the past spikes
    mHasFired = hasFired;
    
    // Vector to store candidates for the winner
    std::vector<Array1d<NLuint>> candidates;
    
    // Propagate spikes and get candidates
    searchCandidates(spike, candidates);
    
    // Choose the winner
    chooseWinner(candidates, winner);

    if (mWinnerFound)
    {
        // Learn the spike pattern
        learn(winner);
        
        // Reset if winner found
        reset();
    }
}

// ------------------------------------------------------------------------------------------------------------------ //

// Initialize weights with Gaussian distribution
void Layer::initWeights(NLfloat mean, NLfloat std)
{
    mWeights.allocate(mMapSize, mMapSize, mReceptiveSize, mReceptiveSize, mNumInputs);
    for (int mapRow = 0; mapRow < mMapSize; mapRow++)
    {
        for (int mapCol = 0; mapCol < mMapSize; mapCol++)
        {
            for (int recRow = 0; recRow < mReceptiveSize; recRow++)
            {
                for (int recCol = 0; recCol < mReceptiveSize; recCol++)
                {
                    for (int inputNo = 0; inputNo < mNumInputs; inputNo++)
                        mWeights[mapRow][mapCol][recRow][recCol][inputNo] = generateGaussian(mean, std);
                }
            }
        }
    }
}

// ------------------------------------------------------------------------------------------------------------------ //

// Increment the potentials and find candidates
void Layer::searchCandidates(Array1d<NLuint> &spike, std::vector<Array1d<NLuint>> &candidates)
{
    for (NLuint row = 0; row < mMapSize; row++)
    {
        for (NLuint col = 0; col < mMapSize; col++)
        {
            // Increment potentials
            mPotentials[row][col] += mWeights[row][col][spike[0]][spike[1]][spike[2]];
            
            // Look for candidates
            addCandidate(mPotentials[row][col], candidates, row, col);
            
            // Keep track of past spikes
            mHasFired[spike[0]][spike[1]][spike[2]] = true;
        }
    }
}

// ------------------------------------------------------------------------------------------------------------------ //

// Check for spikes
void Layer::addCandidate(NLfloat potential, std::vector<Array1d<NLuint>> &candidates, NLuint row, NLuint col)
{
    // If below threhsold, do not fire
    if (potential < mThreshold)
        return;
    
    // Stochastically fire if threshold has exceeded
    if (generateBoolean(potential / mMaxPotential))
    {
        Array1d<NLuint> candidate(2);
        candidate[0] = row;
        candidate[1] = col;
        candidates.emplace_back(candidate);
        return;
    }
    else
        return;
}

// ------------------------------------------------------------------------------------------------------------------ //

// Check if candidates are available, if so choose one randomely
void Layer::chooseWinner(std::vector<Array1d<NLuint>> &candidates, Array1d<NLuint> &winner)
{
    // Candidate available?
    if (candidates.size() == 0)
        return;
    
    // Choose one randomely
    winner = candidates[randomIndex(static_cast<NLuint>(candidates.size()))];
    
    // Winner found
    mWinnerFound = true;
}

// ------------------------------------------------------------------------------------------------------------------ //

void Layer::reset()
{
    // Reset the potential
    for (NLuint row = 0; row < mMapSize; row++)
    {
        for (NLuint col = 0; col < mMapSize; col++)
        {
            mPotentials[row][col] = 0.0f;
        }
    }
    
    // Reset past spikes
    for (NLuint row = 0; row < mReceptiveSize; row++)
    {
        for (NLuint col = 0; col < mReceptiveSize; col++)
        {
            for (NLuint in = 0; in < mNumInputs; in++)
                mHasFired[row][col][in] = false;
        }
    }
}

// ------------------------------------------------------------------------------------------------------------------ //

// Learn the found spike pattern
void Layer::learn(Array1d<NLuint> &winner)
{
    // Find neighbors
    NLuint startRow, endRow, startCol, endCol;
    getRange(startRow, endRow, startCol, endCol, winner[0], winner[1]);
    
    // Update weights of the winner and the neighbors
    for (NLuint row = startRow; row <= endRow; row++)
    {
        for (NLuint col = startCol; col <= endCol; col++)
            updateWeights(row, col);
    }
}

// ------------------------------------------------------------------------------------------------------------------ //

// Getting the neighbor of the winners (crop around the edges)
void Layer::getRange(NLuint &startRow, NLuint &endRow,
                     NLuint &startCol, NLuint &endCol,
                     NLuint winnerRow, NLuint winnerCol)
{
    // Size of the lateral weights
    NLuint weightSize = mLateralSize * 2 + 1;
    
    // Crop rows if necessary
    NLuint distToEnd  = mMapSize - winnerRow;
    startRow = (mLateralSize <= winnerRow) ? winnerRow - mLateralSize : 0;
    endRow   = (mLateralSize < distToEnd) ? winnerRow + mLateralSize : weightSize - (mLateralSize - distToEnd) - 1;
    
    // Crop columns if necessary
    distToEnd = mMapSize - winnerCol;
    startCol  = (mLateralSize <= winnerCol) ? winnerCol - mLateralSize : 0;
    endCol    = (mLateralSize < distToEnd) ? winnerCol + mLateralSize : weightSize - (mLateralSize - distToEnd) - 1;
}

// ------------------------------------------------------------------------------------------------------------------ //

// Learn the past spike pattern
void Layer::updateWeights(NLuint mapRow, NLuint mapCol)
{
    for (NLuint recRow = 0; recRow < mReceptiveSize; recRow++)
    {
        for (NLuint recCol = 0; recCol < mReceptiveSize; recCol++)
        {
            for (NLuint in = 0; in < mNumInputs; in++)
            {
                NLfloat w_ij = mWeights[mapRow][mapCol][recRow][recCol][in];
                
                if (mHasFired[recRow][recCol][in])
                    // Potentiation
                    mWeights[mapRow][mapCol][recRow][recCol][in] += mAPlus * w_ij * (1 - w_ij);
                else
                    // Depression
                    mWeights[mapRow][mapCol][recRow][recCol][in] -= mAMinus * w_ij * (1 - w_ij);
            }
        }
    }
}

// ------------------------------------------------------------------------------------------------------------------ //
