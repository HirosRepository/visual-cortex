#include "../../include/model/visual-input.hpp"
#include "../../include/utils.hpp"
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>

void VisualInput::init(VisualInputParameters params)
{
    // Initialize imageloader
    if (params.mode == LoadMode::FROM_STREAM)
        mImageLoader.init(params.mode);
    else if (params.mode == LoadMode::FROM_VIDEO_FILE)
        mImageLoader.init(params.mode, params.filePath);
    else
        mImageLoader.init(params.mode, params.filePath, params.numFiles, params.numFrames);
    
    // Input image size
    mInputSize = params.inputSize;
    
    // Number of frames kept
    mNumKeptFrames = params.numKeptFrames;
    
    // Size of the receptive field
    mReceptiveSize = params.receptiveSize;
    
    // Thresholds
    mThreshold = static_cast<NLuint>(params.threshold * mReceptiveSize * mReceptiveSize);
    //mThresholds[SpikeType::COARSE_GRAY] = static_cast<NLuint>(params.threshold * mReceptiveSize * mReceptiveSize * 2);
    //mThresholds[SpikeType::COLOR]  = static_cast<NLuint>(params.threshold * mReceptiveSize * mReceptiveSize * 2);
    
    // Which channels are active
    mUseSharpSpikes  = params.useSharpSpipkes;
    mUseCoarseSpikes = params.useCoarseSpikes;
    mUseColorSpikes  = params.useColorSpikes;
    
    // Filter parameters
    mKernelSize = params.kernelSize;
    mSmallSigma = params.smallSigma;
    mLargeSigma = params.largeSigma;
    
    // Threshold for firing in DoG layer
    mDoGThreshold = params.DoGThreshold;
    
    // Initialize past spikes
    mSharpSpikeArray.allocate (mInputSize, mInputSize, mNumKeptFrames);
    mCoarseSpikeArray.allocate(mInputSize, mInputSize, mNumKeptFrames);
    mRGSpikeArray.allocate (mInputSize, mInputSize, mNumKeptFrames);
    mBYSpikeArray.allocate (mInputSize, mInputSize, mNumKeptFrames);
    
    mCurrentFrameIdx = 0;
}

// ------------------------------------------------------------------------------------------------------------------ //

// Load a single frame
void VisualInput::loadFrame()
{
    // Load image
    mImageLoader.loadImage(mCurrentFrame);
    
    // Crop image
    cropImage();
    
    // Reset
    mGrayAvailable = false;
    
    // Generate appropriate spikes
    if (mUseSharpSpikes)
        generateGraySpikes(ConvolutionSize::SHARP);
    if (mUseCoarseSpikes)
        generateGraySpikes(ConvolutionSize::COARSE);
    if (mUseColorSpikes)
        generateColorSpikes();
    
    mCurrentFrameIdx++;
    if (mCurrentFrameIdx == mNumKeptFrames)
        mCurrentFrameIdx = 0;
        
}

// ------------------------------------------------------------------------------------------------------------------ //

// Generate spikes from the image
void VisualInput::generateGraySpikes(ConvolutionSize size)
{
    // Check if grayscale image is already available
    if (!mGrayAvailable)
        cv::cvtColor(mCurrentFrame, mCurrentGray, cv::COLOR_BGR2GRAY);
    
    // ON-OFF and OFF-ON channels
    cv::Mat onOff;
    cv::Mat offOn;
    
    // Convolve with difference of Gaussian
    convolveDoG(onOff, ConvolutionMode::ON_OFF, size);
    convolveDoG(offOn, ConvolutionMode::OFF_ON, size);
    
    // Convert image to spikes
    convertToGraySpikes(onOff, offOn);
    
    // Check for threshold and add to the vector
    checkSharpThreshold();
}

// ------------------------------------------------------------------------------------------------------------------ //

//
void VisualInput::generateColorSpikes()//TEST
{
    
    cv::Mat rg[2];
    cv::Mat by[2];
    
    convertBGR2RGBY(mCurrentFrame, rg, by);
    
    cv::Mat RGOnOff;
    cv::Mat RGOffOn;
    cv::Mat BYOnOff;
    cv::Mat BYOffOn;
    
    convolveColorDoG(rg, RGOnOff, ConvolutionMode::ON_OFF);
    convolveColorDoG(rg, RGOffOn, ConvolutionMode::OFF_ON);
    convolveColorDoG(by, BYOnOff, ConvolutionMode::ON_OFF);
    convolveColorDoG(by, BYOffOn, ConvolutionMode::OFF_ON);
    
    convertToColorSpikes(RGOnOff, RGOffOn, BYOnOff, BYOffOn);
    
    checkColorThreshold(mRGSpikeArray, SpikeType::RED_GREEN);
    checkColorThreshold(mBYSpikeArray, SpikeType::BLUE_YELLOW);
    
    
}

// ------------------------------------------------------------------------------------------------------------------ //

// Crop image to a square ROI at the center
void VisualInput::cropImage()
{
    // Center coordinate
    NLuint centerRow = mCurrentFrame.rows / 2;
    NLuint centerCol = mCurrentFrame.cols / 2;
    
    // Half of side of ROI
    NLuint halfSize = mInputSize / 2;
    
    // Left top corner coordinate
    NLuint startRow = centerRow - halfSize;
    NLuint startCol = centerCol - halfSize;
    
    // Crop image
    cv::Rect roi(startCol, startRow, mInputSize, mInputSize);
    mCurrentFrame = mCurrentFrame(roi);
}

// ------------------------------------------------------------------------------------------------------------------ //

//
void VisualInput::convertBGR2RGBY(cv::Mat &src, cv::Mat rg[], cv::Mat by[])//TEST
{
    cv::Mat bgr[3];
    cv::split(src, bgr);
    
    // Red
    rg[0] = bgr[2] - (bgr[1] + bgr[0]) / 2;
    
    // Green
    rg[1] = bgr[1] - (bgr[2] + bgr[0]) / 2;
    
    // Blue
    by[0] = bgr[0] - (bgr[2] + bgr[1]) / 2;
    
    // Yellow
    by[1] = (bgr[2] + bgr[1]) / 2 - cv::abs(bgr[2] - bgr[1]) / 2 - bgr[0];
}

// ------------------------------------------------------------------------------------------------------------------ //

// Convolve with difference of Gaussian
void VisualInput::convolveDoG(cv::Mat &dst, ConvolutionMode mode, ConvolutionSize size)
{
    // Convolve with Gaussian with different sigma values
    cv::Mat small;
    cv::Mat large;
    cv::GaussianBlur(mCurrentGray, small, cv::Size(mKernelSize, mKernelSize), mSmallSigma);
    cv::GaussianBlur(mCurrentGray, large, cv::Size(mKernelSize, mKernelSize), mLargeSigma);
    
    // Calculate the difference
    if (mode == ConvolutionMode::ON_OFF)
        dst = small - large;//FIXME: should this be cv::abs
    else
        dst = large - small;
    
    // Normalize between 0 and 255
    cv::normalize(dst, dst, 0, 255, cv::NORM_MINMAX);
    
    // Threshold value for spiking
    cv::threshold(dst, dst, mDoGThreshold, 255, cv::THRESH_BINARY);
}

// ------------------------------------------------------------------------------------------------------------------ //

//
void VisualInput::convolveColorDoG(cv::Mat src[], cv::Mat &dst, ConvolutionMode mode)//TEST
{
    cv::Mat small;
    cv::Mat large;
    cv::GaussianBlur(cv::abs(src[0] - src[1]), small, cv::Size(mKernelSize, mKernelSize), mSmallSigma);
    cv::GaussianBlur(cv::abs(src[0] - src[1]), large, cv::Size(mKernelSize, mKernelSize), mLargeSigma);
    
    if (mode == ConvolutionMode::ON_OFF)
        dst = small - large;
    else
        dst = large - small;
    
    
    
    cv::normalize(dst, dst, 0, 255, cv::NORM_MINMAX);
    cv::threshold(dst, dst, mDoGThreshold, 255, cv::THRESH_BINARY);
}

// ------------------------------------------------------------------------------------------------------------------ //

// Convert the image to array of spikes
void VisualInput::convertToGraySpikes(cv::Mat &onOff, cv::Mat &offOn)
{
    // For all neurons: get intensity and if it equals 255 true
    for (NLuint row = 0; row < mInputSize; row++)
    {
        for (NLuint col = 0; col < mInputSize; col++)
        {
            // ON-OFF channels
            uchar intensity0 = onOff.at<uchar>(row, col);
            uchar intensity1 = offOn.at<uchar>(row, col);
            
            if ((intensity0 == 255) && (intensity1 != 255))
                mSharpSpikeArray[row][col][mCurrentFrameIdx] = 0;
            else if ((intensity0 != 255) && (intensity1 == 255))
                mSharpSpikeArray[row][col][mCurrentFrameIdx] = 1;
            else
                mSharpSpikeArray[row][col][mCurrentFrameIdx] = -1;
            
        }
    }
}

// ------------------------------------------------------------------------------------------------------------------ //
void VisualInput::convertToColorSpikes(cv::Mat &RGOnOff, cv::Mat &RGOffOn, cv::Mat &BYOnOff, cv::Mat BYOffOn)
{
    for (NLuint row = 0; row < mInputSize; row++)
    {
        for (NLuint col = 0; col < mInputSize; col++)
        {
            uchar channel0 = RGOnOff.at<uchar>(row, col);
            uchar channel1 = RGOffOn.at<uchar>(row, col);
            uchar channel2 = BYOnOff.at<uchar>(row, col);
            uchar channel3 = BYOffOn.at<uchar>(row, col);
            
            if ((channel0 == 255) && (channel1 != 255))
                mRGSpikeArray[row][col][mNumKeptFrames - 1] = 0;
            else if ((channel0 != 255) && (channel1 == 255))
                mRGSpikeArray[row][col][mNumKeptFrames - 1] = 1;
            else
                mRGSpikeArray[row][col][mNumKeptFrames - 1] = -1;
            
            if ((channel2 == 255) && (channel3 != 255))
                mBYSpikeArray[row][col][mNumKeptFrames - 1] = 0;
            else if ((channel2 != 255) && (channel3 == 255))
                mBYSpikeArray[row][col][mNumKeptFrames - 1] = 1;
            else
                mBYSpikeArray[row][col][mNumKeptFrames - 1] = -1;
        }
    }
}

// ------------------------------------------------------------------------------------------------------------------ //

// Check all neurons if they have enough spikes in the receptive field
void VisualInput::checkSharpThreshold()
{
    // Reset completely
    mSpikes[SpikeType::SHARP_GRAY].clear();
    
    // Distance from the center of the receptive field
    NLuint radius = mReceptiveSize / 2;
    
    // Add spikes to the vector if above threshold
    for (NLuint row = radius; row < mInputSize - radius; row++)
    {
        for (NLuint col = radius; col < mInputSize - radius; col++)
        {
            // If it has not fired at this location, continue
            if (mSharpSpikeArray[row][col][mCurrentFrameIdx] < 0)
                continue;
            
            // Add to the vector
            if (aboveThreshold(row, col, mSharpSpikeArray))
            {
                Array1d<NLuint> location(3);
                location[0] = row;
                location[1] = col;
                location[2] = mSharpSpikeArray[row][col][mCurrentFrameIdx];
                mSpikes[SpikeType::SHARP_GRAY].emplace_back(location);
            }
        }
    }
}

// ------------------------------------------------------------------------------------------------------------------ //

// Check all neurons if they have enough spikes in the receptive field
void VisualInput::checkColorThreshold(Array3d<NLint> &spikeArray, SpikeType type)
{
    
    // Reset completely
    mSpikes[type].clear();
    
    // Distance from the center of the receptive field
    NLuint radius = mReceptiveSize / 2;
    
    // Add spikes to the vector if above threshold
    for (NLuint row = radius; row < mInputSize - radius; row++)
    {
        for (NLuint col = radius; col < mInputSize - radius; col++)
        {
            // If it has not fired at this location, continue
            if (spikeArray[row][col][mCurrentFrameIdx] < 0)
                continue;
            
            // Add to the vector
            Array1d<NLuint> location(3);
            location[0] = row;
            location[1] = col;
            location[2] = spikeArray[row][col][mCurrentFrameIdx];
            mSpikes[type].emplace_back(location);
        }
    }
}

// ------------------------------------------------------------------------------------------------------------------ //

// Check if the the particular neuron exceeds the threshold
bool VisualInput::aboveThreshold(NLuint neuronRow, NLuint neuronCol, Array3d<NLint> &spikeArray)
{
    // Count false instead of true because it’s faster
    NLuint numFalse = 0;
    
    // Distance to the edge
    NLuint radius = mReceptiveSize / 2;
    
    // Count falses in the receptive field
    for (NLuint row = 0; row < mReceptiveSize; row++)
    {
        for (NLuint col = 0; col < mReceptiveSize; col++)
        {
            if (spikeArray[neuronRow + row - radius][neuronCol + col - radius][mCurrentFrameIdx] < 0)
                numFalse++;
            
            if (numFalse > mThreshold)
                return false;
        }
    }
    return true;
}
// ------------------------------------------------------------------------------------------------------------------ //
