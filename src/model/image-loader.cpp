#include "../../include/model/image-loader.hpp"

#include <iostream>

// Initializers ------------------------------------------------------------------------------------------------------ //

// Load from streaming
void ImageLoader::init(LoadMode mode)
{
    mMode = mode;
}

// ------------------------------------------------------------------------------------------------------------------ //

// Load from video stream
void ImageLoader::init(LoadMode mode, std::string filePath)
{
    mMode = mode;
    mFilePath = filePath;
}

// ------------------------------------------------------------------------------------------------------------------ //

// Load from list of files
void ImageLoader::init(LoadMode mode, std::string filePath, GLuint numFiles, GLuint numFrames)
{
    mMode = mode;
    mFilePath = filePath;
    mNumFiles = numFiles;
    mNumFrames = numFiles;
    mCurrentFile = 0;
    mCurrentFrame = 0;
}
// ------------------------------------------------------------------------------------------------------------------ //

// Load single frame
void ImageLoader::loadImage(cv::Mat &frame)
{
    if (mMode == LoadMode::FROM_STREAM)
        loadFromStream(frame);
    else if (mMode == LoadMode::FROM_VIDEO_FILE)
        loadFromVideoFile(frame);
    else
        loadFromImageFiles(frame);
}
// ------------------------------------------------------------------------------------------------------------------ //
void ImageLoader::loadFromStream(cv::Mat &frame)
{
    
}
// ------------------------------------------------------------------------------------------------------------------ //
void ImageLoader::loadFromVideoFile(cv::Mat &frame)
{
    
}
// ------------------------------------------------------------------------------------------------------------------ //

// 
void ImageLoader::loadFromImageFiles(cv::Mat &frame)//TEST
{
    std::string path = mFilePath.substr(0, mFilePath.size() - 4);
    while (std::isdigit(path.back()))
        path = path.substr(0, path.size() - 1);
    
    frame = cv::imread(path + std::to_string(mCurrentFile) + ".jpg");
    
    if (!frame.data)
    {
        std::cout << "Could not open or find the image" << std::endl;
    }
    
    if (mCurrentFrame < mNumFrames)
        mCurrentFrame++;
    else
    {
        mCurrentFrame = 0;
        //mCurrentFile++;
    }
}
