#include "v1.hpp"

// Public functions ------------------------------------------------------------------------------------------------- //
void V1::init(V1Parameters params)
{
    // Pass on hyperparameters
    mPrimaryLayer.init(params.primaryLayerParams);
}

// ------------------------------------------------------------------------------------------------------------------ //
void V1::learn(const Array3d<NNuint> *sharpSpikes)
{
    // Train primary layer
    mPrimaryLayer.learn(sharpSpikes);
}
