#include "../../include/ui/buffer.hpp"

// Getter  ---------------------------------------------------------------------------------------------------------- //
GLuint Buffer::getVAO()
{
    return mVAO;
}

// Member Functions ------------------------------------------------------------------------------------------------- //
GLvoid Buffer::setupBuffer(const std::vector<GLfloat> *vertices, GLenum usage, Array1d<GLuint> dataShape)
{
    // Generate Buffers
    glGenVertexArrays(1, &mVAO);
    glGenBuffers(1, &mVBO);
    
    // Bind VBO
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    
    glBufferData(GL_ARRAY_BUFFER,
                 (*vertices).size() * sizeof(GLfloat),
                 &(*vertices)[0],
                 usage);
    
    // Bind VAO
    glBindVertexArray(mVAO);
    GLuint stride = 0;
    for (int location = 0; location < dataShape.size(); location++)
        stride += dataShape[location];
    long offset = 0;
    
    for (int location = 0; location < dataShape.size(); location++)
    {
        GLuint size = dataShape[location];
        glEnableVertexAttribArray(location);
        glVertexAttribPointer(location,                 // index
                              size,                     // size
                              GL_FLOAT,                 // type
                              GL_FALSE,                 // normalized
                              stride * sizeof(GLfloat), // stride
                              (void*) (offset));        // pointer
        offset += size * sizeof(GLfloat);
    }
    
    // Unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}
