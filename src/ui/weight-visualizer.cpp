#include "../../include/ui/weight-visualizer.hpp"
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

// ------------------------------------------------------------------------------------------------------------------ //
void WeightVisualizer::init(Gui &gui, const GLchar *vShaderPath, const GLchar *fShaderPath, GLuint marginSize, GLfloat gap)
{
    // Window shape
    mWidth  = gui.getWidth();
    mHeight = gui.getHeight();
    
    mMarginSize = marginSize;
    mGap = gap;
    
    // Top left corner location
    mRow = mMarginSize;
    mCol = mMarginSize;
    
    // Sizes
    uint smaller = (mWidth > mHeight) ? mHeight : mWidth;   // smaller side
    mSize = static_cast<uint>(smaller - 2 * mMarginSize);

    // Initialize vertices
    initVertices();
    
    // Data shape
    mDataShape.allocate(1);
    mDataShape[0] = 4;      // vec2 for location + vec2 for texture
    
    // Shader
    mSquareShader.loadShader(vShaderPath, fShaderPath);
    
    // Heatmap
    mHighColor = glm::vec3(0.8f, 0.2f, 0.0f);
    mLowColor  = glm::vec3(0.0f, 0.3f, 1.0f);
}


// ------------------------------------------------------------------------------------------------------------------ //

// Initialize the vertices of the square
void WeightVisualizer::initVertices()
{
    
    GLfloat vertices[] =
    {
        // Pos          // Tex
        0.0f,   1.0f,   0.0f,   1.0f,
        1.0f,   0.0f,   1.0f,   0.0f,
        0.0f,   0.0f,   0.0f,   0.0f,
        
        0.0f,   1.0f,   0.0f,   1.0f,
        1.0f,   1.0f,   1.0f,   1.0f,
        1.0f,   0.0f,   1.0f,   0.0f
    };
    
    uint size = 6 * 4;
    for (uint idx = 0; idx < size; idx++)
        mSquareVertices.emplace_back(vertices[idx]);
}

// ------------------------------------------------------------------------------------------------------------------ //

// Draw all weights
void WeightVisualizer::draw(Array5d<float> &weights)
{
    mMapSize = weights.shape()[0];
    mReceptiveSize = weights.shape()[2];
    
    for (uint mapRow = 0; mapRow < mMapSize; mapRow++)
    {
        for (uint mapCol = 0; mapCol < mMapSize; mapCol++)
        {
            for (uint recRow = 0; recRow < mReceptiveSize; recRow++)
            {
                for (uint recCol = 0; recCol < mReceptiveSize; recCol++)
                {
                    float value = weights[mapRow][mapCol][recRow][recCol][0];
                    drawSingle(mapRow, mapCol, recRow, recCol, value);
                }
            }
        }
    }
}

// ------------------------------------------------------------------------------------------------------------------ //

// Clean up
void WeightVisualizer::clear()//TEST
{
    
}

// ------------------------------------------------------------------------------------------------------------------ //

// Draw a single square
void WeightVisualizer::drawSingle(uint mapRow, uint mapCol, uint recRow, uint recCol, float value)//TEST
{
    // Set shader
    mSquareShader.use();
    
    // Calculate location of the square
    float squareSize;
    glm::vec2 location = calculateLocation(mapRow, mapCol, recRow, recCol, squareSize);
    
    // Calculate the projection matrix
    calculateProjection(location);
    
    // Transformation
    glm::mat4 model;
    model = glm::translate(model, glm::vec3(location, 0.0f));
    model = glm::scale(model, glm::vec3(squareSize, squareSize, 1.0f));
    
    glm::vec3 color = calculateColor(value);
    
    // Set the uniform
    mSquareShader.setUniformMat4 ("projection", mProjection);
    mSquareShader.setUniformMat4("model", model);
    //mSquareShader.setUniformFloat("brightness", value);
    mSquareShader.setUniformVec3("color", color);
    
    // Send the vertices to the buffer
    mSquareBuffer.setupBuffer(&mSquareVertices, GL_DYNAMIC_DRAW, mDataShape);
    
    // Draw square
    glBindVertexArray(mSquareBuffer.getVAO());
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

// ------------------------------------------------------------------------------------------------------------------ //

// Calculate the location of the top left corner of the square
glm::vec2 WeightVisualizer::calculateLocation(uint mapRow, uint mapCol, uint recRow, uint recCol, float &squareSize)
{
    // Cast uint to float
    float mapRowf = static_cast<float> (mapRow);
    float mapColf = static_cast<float> (mapCol);
    float recRowf = static_cast<float> (recRow);
    float recColf = static_cast<float> (recCol);
    float size    = static_cast<float> (mSize);
    float mapSize = static_cast<float> (mMapSize);
    float recSize = static_cast<float> (mReceptiveSize);
    float row     = static_cast<float> (mRow);
    float col     = static_cast<float> (mCol);
    
    // Calculate the position of the square representing the neuron
    float neuronSize = size / mapSize;
    float neuronRow = static_cast<float>(mRow + mapRowf * neuronSize);
    float neuronCol = static_cast<float>(mCol + mapColf * neuronSize);
    
    // Calculate the position of the square location
    squareSize = (neuronSize - mGap) / recSize;//FIXME: this will afftec the size of margin
    row = static_cast<float>(neuronRow + recRowf * squareSize);
    col = static_cast<float>(neuronCol + recColf *squareSize);
    
    // Make a GLM vector
    glm::vec2 location = glm::vec2(row, col);
    
    return location;
}

// ------------------------------------------------------------------------------------------------------------------ //
void WeightVisualizer::calculateProjection(glm::vec2 location)//TEST
{
    // Set Projection Matrix
    mProjection = glm::ortho(0.0f,                                      // left
                             static_cast<GLfloat>(mWidth),              // right
                             static_cast<GLfloat>(mHeight),             // bottom
                             0.0f,                                      // top
                             -1.0f,                                     // zNear
                             1.0f);                                     // zFar
}


glm::vec3 WeightVisualizer::calculateColor(float value)
{
    float alpha = 1 - value;
    glm::vec3 color =  alpha * mHighColor + (1.0f - alpha) * mLowColor;
    
    return color;
}
