#include "../../include/ui/shader.hpp"

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>


// Set Shader to Active
Shader &Shader::use()
{
    glUseProgram(this->ID);
    return *this;
}


// Uniform Setters: sends values to uniform variables in shader ----------------------------------------------------- //

GLvoid Shader::setUniformInt(const GLchar *name, GLint value, GLboolean useShader)
{
    if (useShader)
        this->use();
    glUniform1i(glGetUniformLocation(this->ID, name), value);
}


GLvoid Shader::setUniformFloat(const GLchar *name, GLfloat value, GLboolean useShader)
{
    if (useShader)
        this->use();
    glUniform1f(glGetUniformLocation(this->ID, name), value);
}

GLvoid Shader::setUniformVec2(const GLchar *name, GLfloat x, GLfloat y, GLboolean useShader)
{
    if (useShader)
        this->use();
    glUniform2f(glGetUniformLocation(this->ID, name), x, y);
}

GLvoid Shader::setUniformVec2(const GLchar *name, glm::vec2 &value, GLboolean useShader)
{
    if (useShader)
        this->use();
    glUniform2f(glGetUniformLocation(this->ID, name), value.x, value.y);
}

GLvoid Shader::setUniformVec3(const GLchar *name, GLfloat x, GLfloat y, GLfloat z, GLboolean useShader)
{
    if (useShader)
        this->use();
    glUniform3f(glGetUniformLocation(this->ID, name), x, y, z);
}

GLvoid Shader::setUniformVec3(const GLchar *name, glm::vec3 &value, GLboolean useShader)
{
    if (useShader)
        this->use();
    glUniform3f(glGetUniformLocation(this->ID, name), value.x, value.y, value.z);
}

GLvoid Shader::setUniformVec4(const GLchar *name,
                              GLfloat x, GLfloat y, GLfloat z, GLfloat w,
                              GLboolean useShader)
{
    if (useShader)
        this->use();
    glUniform4f(glGetUniformLocation(this->ID, name), x, y, z, w);
}

GLvoid Shader::setUniformVec4(const GLchar *name, glm::vec4 &value, GLboolean useShader)
{
    if (useShader)
        this->use();
    glUniform4f(glGetUniformLocation(this->ID, name), value.x, value.y, value.z, value.w);
}

GLvoid Shader::setUniformMat4(const GLchar *name, glm::mat4 &value, GLboolean useShader)
{
    if (useShader)
        this->use();
    glUniformMatrix4fv(glGetUniformLocation(this->ID, name), 1, GL_FALSE, glm::value_ptr(value));
}

// Loads and Compiles Shader from Source Code
GLvoid Shader::loadShader(const GLchar *vShaderPath, const GLchar *fShaderPath, const GLchar *gShaderPath)
{
    // Retrieve the Vertex and Fragment Source Code from the Filepath
    std::string vertexCode;
    std::string fragmentCode;
    std::string geometryCode;
    
    try
    {
        // Open Files
        std::ifstream vertexShaderFile(vShaderPath);
        std::ifstream fragmentShaderFile(fShaderPath);
        std::stringstream vShaderStream, fShaderStream;
        
        // Read File's Buffer Contents into Streams
        vShaderStream << vertexShaderFile.rdbuf();
        fShaderStream << fragmentShaderFile.rdbuf();
        
        // Close File Handlers
        vertexShaderFile.close();
        fragmentShaderFile.close();
        
        // Convert Stream into String
        vertexCode      = vShaderStream.str();
        fragmentCode    = fShaderStream.str();
        
        // If Geometry Shader Path is Present, Load it as Well
        if (gShaderPath != nullptr)
        {
            std::ifstream geometryShaderFile(gShaderPath);
            std::stringstream gShaderStream;
            gShaderStream << geometryShaderFile.rdbuf();
            geometryShaderFile.close();
            geometryCode = gShaderStream.str();
        }
    }
    catch (std::exception e)
    {
        std::cout << "ERROR::SHADER: FAILED TO READ SHADER FILES" << std::endl;
    }
    
    const GLchar *vShaderCode = vertexCode.c_str();
    const GLchar *fShaderCode = fragmentCode.c_str();
    const GLchar *gShaderCode = geometryCode.c_str();
    
    // Create Shader Object from Source Code
    compile(vShaderCode, fShaderCode, gShaderPath != nullptr ? gShaderCode : nullptr);
}

// Compile Shader
GLvoid Shader::compile(const GLchar *vertexSource,
                       const GLchar *fragmentSource,
                       const GLchar *geometrySource)
{
    // Handle names
    GLuint sVertex, sFragment, sGeometry;
    
    // Vertex Shader
    sVertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(sVertex, 1, &vertexSource, NULL);
    glCompileShader(sVertex);
    checkCompileErrors(sVertex, "VERTEX");
    
    // Fragment Shader
    sFragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(sFragment, 1, &fragmentSource, NULL);
    glCompileShader(sFragment);
    checkCompileErrors(sFragment, "FRAGMENT");
    
    // Geometry Shader (optional)
    if (geometrySource != nullptr)
    {
        sGeometry = glCreateShader(GL_GEOMETRY_SHADER);
        glShaderSource(sGeometry, 1, &geometrySource, NULL);
        glCompileShader(sGeometry);
        checkCompileErrors(sGeometry, "GEOMETRY");
    }
    
    // Compile Shader Program
    this->ID = glCreateProgram();
    glAttachShader(this->ID, sVertex);
    glAttachShader(this->ID, sFragment);
    if (geometrySource != nullptr)
        glAttachShader(this->ID, sGeometry);
    glLinkProgram(this->ID);
    checkCompileErrors(this->ID, "PROGRAM");
    
    // Delete shaders
    glDeleteShader(sVertex);
    glDeleteShader(sFragment);
    if (geometrySource != nullptr)
        glDeleteShader(sGeometry);
}

// Check for Compile Errors ----------------------------------------------------------------------------------------- //

GLvoid Shader::checkCompileErrors(GLuint object, std::string type)
{
    GLint success;
    GLchar infoLog[1024];
    
    if (type != "PROGRAM")  // check for compile-time errors
    {
        glGetShaderiv(object, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            glGetShaderInfoLog(object, 1024, NULL, infoLog);
            std::cout << "| ERROR::SHADER: COMPILE-TIME ERROR: TYPE: " << type << "\n"
            << infoLog << "\n -- --------------------------------------------------- -- "
            << std::endl;
        }
    }
    else    // check for link-time errors
    {
        glGetProgramiv(object, GL_LINK_STATUS, &success);
        if (!success)
        {
            glGetProgramInfoLog(object, 1024, NULL, infoLog);
            std::cout << "| ERROR::SHADER: LINK-TIME ERROR: TYPE: " << type << "\n"
            << infoLog << "\n -- --------------------------------------------------- -- "
            << std::endl;
        }
    }
}
