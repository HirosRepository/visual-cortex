#include "../../include/ui/gui.hpp"


// Constructors ------------------------------------------------------------------------------------------- //
Gui::Gui(GLchar *name, GLuint width, GLuint height, GLuint major, GLuint minor, GLboolean resizable)
{
    // Initialize GLFW
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // needed for Mac
    glfwWindowHint(GLFW_RESIZABLE, resizable);
    
    // Create window
    mWindow = glfwCreateWindow(width, height, name, nullptr, nullptr);
    glfwMakeContextCurrent(mWindow);
    
    int screenWidth, screenHeight;
    glfwGetFramebufferSize(getWindow(), &screenWidth, &screenHeight);
    
    // Initialize GLEW
    glewExperimental = GL_TRUE;
    glewInit();
    glGetError();
    
    glViewport(0, 0, screenWidth, screenHeight);
    
    // Set Window Size
    mWidth    = screenWidth;
    mHeight   = screenHeight;
    
    // TODO: Set up KeyCallbacks
    //glfwSetKeyCallback(this->window, this->keyCallback); //FIXME: how to do this?
    
    // TODO: OpenGL Configurations
    glViewport(0, 0, mWidth, mHeight);
}

// Getters ------------------------------------------------------------------------------------------------ //
GLuint Gui::getWidth()
{
    return mWidth;
}

GLuint Gui::getHeight()
{
    return mHeight;
}

GLFWwindow* Gui::getWindow()
{
    return mWindow;
}

// Setters ---------------------------------------------------------------------------------------------------------- //

// Member Functions ------------------------------------------------------------------------------------------------- //
void Gui::keyCallback(GLFWwindow *window, int key, int scanmode, int action, int mode)
{
    // Press esc to Close
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}
