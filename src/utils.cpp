#include "../include/utils.hpp"
#include <cmath>
#include <random>


unsigned int seed = static_cast<unsigned int> (std::chrono::system_clock::now().time_since_epoch().count());
std::default_random_engine generator(seed);

// Generates value between 0 and 1 using Gaussian distribution
NLfloat generateGaussian(NLfloat mean, NLfloat std)
{
    std::normal_distribution<NLfloat> gaussian(mean, std);
    return gaussian(generator);
}

// Generates value between 0 and 1 using uniform distribution
NLfloat generateUiform()
{
    std::uniform_real_distribution<NLfloat> uniform(0.0f, 1.0f);
    return uniform(generator);
}

// Generate true or false based on the probability
bool generateBoolean(NLfloat p)
{
    std::uniform_real_distribution<NLfloat> uniform(0.0f, 1.0f);
    NLfloat value = uniform(generator);
    
    if (value < p)
        return true;
    else
        return false;
}

// Generates random index for an array
NLuint randomIndex(NLuint size)
{
    std::uniform_int_distribution<NLuint> uniform(0, size - 1);
    return uniform(generator);
}

// Generates random index for an array: inclusive
NLuint randomIndex(NLuint start, NLuint end)
{
    std::uniform_int_distribution<NLuint> uniform(start, end);
    return uniform(generator);
}

// Shuffles a vector
void shuffle(std::vector<NLuint> &vector)
{
    std::shuffle(std::begin(vector), std::end(vector), generator);
}
