#include "../include/arrays.hpp"
#include <cassert>
// ------------------------------------------------------------------------------------------------------------------ //
// 1D ARRAY                                                                                                           //
// ------------------------------------------------------------------------------------------------------------------ //


// Constructors ----------------------------------------------------------------------------------------------------- //

// Construct from size only: no initialization!! FIXME: should I add initialization?
template <class T>
Array1d<T>::Array1d(uint size)
{
    mElements = new T[size];
    mSize = size;
}

template Array1d <uint> ::Array1d(uint size);
template Array1d <float>::Array1d(uint size);
template Array1d <bool> ::Array1d(uint size);

// ------------------------------------------------------------------------------------------------------------------ //

// Construct from size and a single value to fill with
template <class T>
Array1d<T>::Array1d(uint size, T value)
{
    mElements = new T[size];
    
    for (uint idx = 0; idx < size; idx++)
        mElements[idx] = value;
    
    mSize = size;
}

template Array1d <uint> ::Array1d(uint size, uint  value);
template Array1d <float>::Array1d(uint size, float value);
template Array1d <bool> ::Array1d(uint size, bool  value);

// ------------------------------------------------------------------------------------------------------------------ //

// Copy constructor
template <class T>
Array1d<T>::Array1d(Array1d<T> &array)
{
    mSize = array.size();
    mElements = new T[mSize];
    
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] = array[idx];
}

template Array1d <uint> ::Array1d(Array1d <uint>  &array);
template Array1d <float>::Array1d(Array1d <float> &array);
template Array1d <bool> ::Array1d(Array1d <bool>  &array);

// ------------------------------------------------------------------------------------------------------------------ //

// Const
template <class T>
Array1d<T>::Array1d(const Array1d<T> &array)
{
    mSize = array.size();
    mElements = new T[mSize];
    
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] = array[idx];
}

template Array1d <uint> ::Array1d(const Array1d <uint>  &array);
template Array1d <float>::Array1d(const Array1d <float> &array);
template Array1d <bool> ::Array1d(const Array1d <bool>  &array);

// Destructor ------------------------------------------------------------------------------------------------------- //

template <class T>
Array1d<T>::~Array1d()
{
    if (!mElements)
        delete [] mElements;
}

template Array1d<uint>::~Array1d();
template Array1d<float>::~Array1d();
template Array1d<bool>::~Array1d();

// Operators -------------------------------------------------------------------------------------------------------- //

// Copy assignment
template <class T>
void Array1d<T>::operator= (Array1d<T> array)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] = array[idx];
}

template void Array1d <uint> ::operator= (Array1d<uint>  array);
template void Array1d <float>::operator= (Array1d<float> array);
template void Array1d <bool> ::operator= (Array1d<bool>  array);

// ------------------------------------------------------------------------------------------------------------------ //

// Assignment by array
template <class T>
void Array1d<T>::operator= (T *array)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] = array[idx];
}

template void Array1d <uint> ::operator= (uint  *array);
template void Array1d <float>::operator= (float *array);
template void Array1d <bool> ::operator= (bool  *array);

// ------------------------------------------------------------------------------------------------------------------ //

// Subscript operator
template <class T>
T& Array1d<T>::operator[] (uint index)
{
    return mElements[index];
}

template uint&  Array1d<uint> ::operator[] (uint index);
template float& Array1d<float>::operator[] (uint index);
template bool&  Array1d<bool> ::operator[] (uint index);

// ------------------------------------------------------------------------------------------------------------------ //

// Return as constant
template <class T>
const T& Array1d<T>::operator[] (uint index) const
{
    return mElements[index];
}

template const uint&  Array1d<uint> ::operator[] (uint index) const;
template const float& Array1d<float>::operator[] (uint index) const;
template const bool&  Array1d<bool> ::operator[] (uint index) const;

// Inplace operators ------------------------------------------------------------------------------------------------ //

// Add by uint
template <class T>
void Array1d<T>::operator+= (uint value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] += value;
}

template void Array1d<uint> ::operator+= (uint value);
template void Array1d<float>::operator+= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract by uint
template <class T>
void Array1d<T>::operator-= (uint value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] -= value;
}

template void Array1d<float>::operator-= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by uint
template <class T>
void Array1d<T>::operator*= (uint value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] *= value;
}

template void Array1d <uint>::operator*= (uint value);
template void Array1d<float>::operator*= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by uint
template <class T>
void Array1d<T>::operator/= (uint value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] /= value;
}

template void Array1d<float>::operator/= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Add by int
template <class T>
void Array1d<T>::operator+= (int value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] += value;
}

template void Array1d<uint> ::operator+= (int value);
template void Array1d<float>::operator+= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract by int
template <class T>
void Array1d<T>::operator-= (int value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] -= value;
}

template void Array1d<int>  ::operator-= (int value);
template void Array1d<float>::operator-= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by int
template <class T>
void Array1d<T>::operator*= (int value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] *= value;
}

template void Array1d <uint>::operator*= (int value);
template void Array1d<float>::operator*= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by int
template <class T>
void Array1d<T>::operator/= (int value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] /= value;
}

template void Array1d<float>::operator/= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Add by float
template <class T>
void Array1d<T>::operator+= (float value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] += value;
}

template void Array1d<float>::operator+= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract by float
template <class T>
void Array1d<T>::operator-= (float value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] -= value;
}

template void Array1d<float>::operator-= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by float
template <class T>
void Array1d<T>::operator*= (float value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] *= value;
}

template void Array1d<float>::operator*= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by float
template <class T>
void Array1d<T>::operator/= (float value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] /= value;
}

template void Array1d<float>::operator/= (float value);

// Inplace elementwise operators ------------------------------------------------------------------------------------ //

// Add uint array
template <class T>
void Array1d<T>::operator+= (Array1d<uint> &array)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] += array[idx];
}

template void Array1d<uint> ::operator+= (Array1d<uint> &array);
template void Array1d<float>::operator+= (Array1d<uint> &array);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract uint array
template <class T>
void Array1d<T>::operator-= (Array1d<uint> &array)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] -= array[idx];
}

template void Array1d<float>::operator-= (Array1d<uint> &array);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply uint array
template <class T>
void Array1d<T>::operator*= (Array1d<uint> &array)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] *= array[idx];
}

template void Array1d<uint>::operator*=  (Array1d<uint> &array);
template void Array1d<float>::operator*= (Array1d<uint> &array);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide uint array
template <class T>
void Array1d<T>::operator/= (Array1d<uint> &array)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] /= array[idx];
}

template void Array1d<float>::operator/= (Array1d<uint> &array);

// ------------------------------------------------------------------------------------------------------------------ //

// Add float array
template <class T>
void Array1d<T>::operator+= (Array1d<float> &array)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] += array[idx];
}

template void Array1d<float>::operator+= (Array1d<float> &array);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract float array
template <class T>
void Array1d<T>::operator-= (Array1d<float> &array)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] -= array[idx];
}

template void Array1d<float>::operator-= (Array1d<float> &array);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by float array
template <class T>
void Array1d<T>::operator*= (Array1d<float> &array)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] *= array[idx];
}

template void Array1d<float>::operator*= (Array1d<float> &array);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by float array
template <class T>
void Array1d<T>::operator/= (Array1d<float> &array)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] /= array[idx];
}

template void Array1d<float>::operator/= (Array1d<float> &array);

// ------------------------------------------------------------------------------------------------------------------ //

// Return the size of the array
template <class T>
const uint Array1d<T>::size() const
{
    return mSize;
}

template const uint Array1d <uint> ::size() const;
template const uint Array1d <float>::size() const;
template const uint Array1d <bool> ::size() const;

// ------------------------------------------------------------------------------------------------------------------ //

// Allocate memory (only once)
template <class T>
void Array1d<T>::allocate(uint size)
{
    // Allocate only if it’s not yet allocated
    assert (!mElements);
    
    mElements = new T[size];
    
    mSize = size;
}

template void Array1d <uint> ::allocate(uint size);
template void Array1d <float>::allocate(uint size);
template void Array1d <bool> ::allocate(uint size);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by uint value
template <class T>
void Array1d<T>::fill(uint value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] = value;
}

template void Array1d <uint> ::fill(uint value);
template void Array1d <float>::fill(uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by int value
template <class T>
void Array1d<T>::fill(int value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] = value;
}

template void Array1d <uint> ::fill(int value);
template void Array1d <float>::fill(int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by float value
template <class T>
void Array1d<T>::fill(float value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] = value;
}

template void Array1d<float>::fill(float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by boolean value
template <class T>
void Array1d<T>::fill(bool value)
{
    for (uint idx = 0; idx < mSize; idx++)
        mElements[idx] = value;
}

template void Array1d<bool>::fill(bool value);

// ------------------------------------------------------------------------------------------------------------------ //

// Get the slice of the array
template <class T>
void Array1d<T>::slice(uint begin, uint end, Array1d<T> &array)
{
    array.allocate(end - begin + 1);
    
    for (uint idx = begin; idx <= end; idx++)
        array[idx - begin] = mElements[idx];
}

template void Array1d <uint> ::slice(uint begin, uint end, Array1d<uint>  &array);
template void Array1d <float>::slice(uint begin, uint end, Array1d<float> &array);
template void Array1d <bool> ::slice(uint begin, uint end, Array1d<bool>  &array);


// ------------------------------------------------------------------------------------------------------------------ //
// 2D ARRAY                                                                                                           //
// ------------------------------------------------------------------------------------------------------------------ //

// Construct from shape
template <class T>
Array2d<T>::Array2d(uint size0, uint size1)
{
    // Allocate axis 0
    mElements = new T*[size0];
    
    // Allocate axis 1
    for (uint ax0 = 0; ax0 < size0; ax0++)
        mElements[ax0] = new T[size1];
    
    // Total number of elements
    mSize = size0 * size1;
    
    // Shape of the array
    mShape.allocate(2);
    mShape[0] = size0;
    mShape[1] = size1;
}

template Array2d <uint> ::Array2d(uint size0, uint size1);
template Array2d <float>::Array2d(uint size0, uint size1);
template Array2d <bool> ::Array2d(uint size0, uint size1);

// ------------------------------------------------------------------------------------------------------------------ //

// Constructor from shape and value
template <class T>
Array2d<T>::Array2d(uint size0, uint size1, T value)
{
    // Allocate axis 0
    mElements = new T*[size0];
    
    // Allocate axis 1 and initialize with value
    for (uint ax0 = 0; ax0 < size0; ax0++)
    {
        mElements[ax0] = new T[size1];
        for (uint ax1 = 0; ax1 < size1; ax1++)
            mElements[ax0][ax1] = value;
    }
    
    // Total number of elements
    mSize = size0 * size1;
    
    // Shape of the array
    mShape.allocate(2);
    mShape[0] = size0;
    mShape[1] = size1;
}

template Array2d <uint> ::Array2d(uint size0, uint size1, uint value);
template Array2d <float>::Array2d(uint size0, uint size1, float value);
template Array2d <bool> ::Array2d(uint size0, uint size1, bool value);

// ------------------------------------------------------------------------------------------------------------------ //

// Destructor
template <class T>
Array2d<T>::~Array2d()
{
    if (mElements)
    {
        for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        {
            if (mElements[ax0])
                delete[] mElements[ax0];
        }
        delete[] mElements;
    }
}

template Array2d <uint> ::~Array2d();
template Array2d <float>::~Array2d();
template Array2d <bool> ::~Array2d();

// Operators -------------------------------------------------------------------------------------------------------- //

// Copy assignment
template <class T>
void Array2d<T>::operator= (Array2d<T> &array2d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] = array2d[ax0][ax1];
}

template void Array2d <uint> ::operator= (Array2d <uint>  &array2d);
template void Array2d <float>::operator= (Array2d <float> &array2d);
template void Array2d <bool> ::operator= (Array2d <bool>  &array2d);

// ------------------------------------------------------------------------------------------------------------------ //

// Copy assignment
template <class T>
void Array2d<T>::operator= (T **array2d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] = array2d[ax0][ax1];
}

template void Array2d <uint> ::operator= (uint  **array2d);
template void Array2d <float>::operator= (float **array2d);
template void Array2d <bool> ::operator= (bool  **array2d);

// ------------------------------------------------------------------------------------------------------------------ //

// Subscript operator
template <class T>
T* Array2d<T>::operator[] (uint index)
{
    return mElements[index];
}

template uint*  Array2d <uint> ::operator[] (uint index);
template float* Array2d <float>::operator[] (uint index);
template bool*  Array2d <bool> ::operator[] (uint index);

// ------------------------------------------------------------------------------------------------------------------ //

// Return as constant
template <class T>
const T* Array2d<T>::operator[] (uint index) const
{
    return mElements[index];
}

template const uint*  Array2d <uint> ::operator[] (uint index) const;
template const float* Array2d <float>::operator[] (uint index) const;
template const bool*  Array2d <bool> ::operator[] (uint index) const;

// Inplace operators ------------------------------------------------------------------------------------------------ //

// Add by uint
template <class T>
void Array2d<T>::operator+= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] += value;
}

template void Array2d <uint> ::operator+= (uint value);
template void Array2d <float>::operator+= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract by uint
template <class T>
void Array2d<T>::operator-= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] -= value;
}

template void Array2d <float>::operator-= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by uint
template <class T>
void Array2d<T>::operator*= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] *= value;
}

template void Array2d <uint> ::operator*= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by uint
template <class T>
void Array2d<T>::operator/= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] /= value;
}

template void Array2d <uint> ::operator/= (uint value);
template void Array2d <float>::operator/= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Add by int
template <class T>
void Array2d<T>::operator+= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] += value;
}

template void Array2d <uint> ::operator+= (int value);
template void Array2d <float>::operator+= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract by int
template <class T>
void Array2d<T>::operator-= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] -= value;
}

template void Array2d <float>::operator-= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by int
template <class T>
void Array2d<T>::operator*= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] *= value;
}

template void Array2d <uint> ::operator*= (int value);
template void Array2d <float>::operator*= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by int
template <class T>
void Array2d<T>::operator/= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] /= value;
}

template void Array2d <float>::operator/= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Add by float
template <class T>
void Array2d<T>::operator+= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] += value;
}

template void Array2d<float>::operator+= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract by float
template <class T>
void Array2d<T>::operator-= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] -= value;
}

template void Array2d<float>::operator-= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by float
template <class T>
void Array2d<T>::operator*= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] *= value;
}

template void Array2d<float>::operator*= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by float
template <class T>
void Array2d<T>::operator/= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] /= value;
}

template void Array2d<float>::operator/= (float value);

// Inplace elementwise operators ------------------------------------------------------------------------------------ //

// Add uint array
template <class T>
void Array2d<T>::operator+= (Array2d<uint> &array2d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] += array2d[ax0][ax1];
}

template void Array2d <uint> ::operator+= (Array2d<uint> &array2d);
template void Array2d <float>::operator+= (Array2d<uint> &array2d);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract uint array
template <class T>
void Array2d<T>::operator-= (Array2d<uint> &array2d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] -= array2d[ax0][ax1];
}

template void Array2d <float>::operator-= (Array2d<uint> &array2d);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply uint array
template <class T>
void Array2d<T>::operator*= (Array2d<uint> &array2d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] *= array2d[ax0][ax1];
}

template void Array2d <uint> ::operator*= (Array2d<uint> &array2d);
template void Array2d <float>::operator*= (Array2d<uint> &array2d);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide uint array
template <class T>
void Array2d<T>::operator/= (Array2d<uint> &array2d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] /= array2d[ax0][ax1];
}

template void Array2d<float>::operator/= (Array2d<uint> &array2d);

// ------------------------------------------------------------------------------------------------------------------ //

// Add float array
template <class T>
void Array2d<T>::operator+= (Array2d<float> &array2d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] += array2d[ax0][ax1];
}

template void Array2d<float>::operator+= (Array2d<float> &array2d);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract float array
template <class T>
void Array2d<T>::operator-= (Array2d<float> &array2d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] -= array2d[ax0][ax1];
}

template void Array2d<float>::operator-= (Array2d<float> &array2d);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by float array
template <class T>
void Array2d<T>::operator*= (Array2d<float> &array2d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] *= array2d[ax0][ax1];
}

template void Array2d<float>::operator*= (Array2d<float> &array2d);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by float array
template <class T>
void Array2d<T>::operator/= (Array2d<float> &array2d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] /= array2d[ax0][ax1];
}

template void Array2d<float>::operator/= (Array2d<float> &array2d);

// ------------------------------------------------------------------------------------------------------------------ //

// Return the total number of elements
template <class T>
const uint Array2d<T>::size() const
{
    return mSize;
}

template const uint Array2d <uint> ::size() const;
template const uint Array2d <float>::size() const;
template const uint Array2d <bool> ::size() const;

// ------------------------------------------------------------------------------------------------------------------ //

// Return the shape
template <class T>
const Array1d<uint> Array2d<T>::shape() const
{
    return mShape;
}

template const Array1d<uint> Array2d <uint> ::shape() const;
template const Array1d<uint> Array2d <float>::shape() const;
template const Array1d<uint> Array2d <bool> ::shape() const;

// ------------------------------------------------------------------------------------------------------------------ //

// Allocate memory (only once)
template <class T>
void Array2d<T>::allocate(Array1d<uint> shape)
{
    // Allocate only if it’s not yet allocated
    assert (!mElements);
    
    // Allocate axis 0
    mElements = new T*[shape[0]];
    
    // Allocate axis 1
    for (uint ax0 = 0; ax0 < shape[0]; ax0++)
        mElements[ax0] = new T[shape[1]];
    
    // Total number of the array
    mSize = shape[0] * shape[1];
    
    // Shape of the array
    mShape = shape;
}

template void Array2d <uint> ::allocate(Array1d <uint>  shape);
template void Array2d <float>::allocate(Array1d <uint>  shape);
template void Array2d <bool> ::allocate(Array1d <uint>  shape);

// ------------------------------------------------------------------------------------------------------------------ //

// Allocate memory (only once)
template <class T>
void Array2d<T>::allocate(uint size0, uint size1)
{
    // Allocate only if it’s not yet allocated
    assert (!mElements);
    
    // Allocate axis 0
    mElements = new T*[size0];
    
    // Allocate axis 1
    for (uint ax0 = 0; ax0 < size0; ax0++)
        mElements[ax0] = new T[size1];
    
    // Total number of the array
    mSize = size0 * size1;
    
    // Shape of the array
    mShape[0] = size0;
    mShape[1] = size1;
}

template void Array2d <uint> ::allocate(uint size0, uint size1);
template void Array2d <float>::allocate(uint size0, uint size1);
template void Array2d <bool> ::allocate(uint size0, uint size1);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by uint value
template <class T>
void Array2d<T>::fill(uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] = value;
}

template void Array2d <uint> ::fill(uint value);
template void Array2d <float>::fill(uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by int value
template <class T>
void Array2d<T>::fill(int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] = value;
}

template void Array2d <uint> ::fill(int value);
template void Array2d <float>::fill(int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by float value
template <class T>
void Array2d<T>::fill(float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] = value;
}

template void Array2d <float>::fill(float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by boolean value
template <class T>
void Array2d<T>::fill(bool value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            mElements[ax0][ax1] = value;
}

template void Array2d<bool>::fill(bool value);

// ------------------------------------------------------------------------------------------------------------------ //

// Get the slice of the array
template <class T>
void Array2d<T>::slice(uint begin0, uint end0,
                       uint begin1, uint end1,
                       Array2d<T> &array2d)
{
    array2d.allocate(end0 - begin0 + 1, end1 - begin1 + 1);
    
    for (uint ax0 = begin0; ax0 <= end0; ax0++)
        for (uint ax1 = begin1; ax1 <= end1; ax1++)
            array2d[ax0 - begin0][ax1 - begin1] = mElements[ax0][ax1];
}

template void Array2d<uint>::slice(uint begin0, uint end0, uint begin1, uint end1, Array2d<uint> &array2d);
template void Array2d<float>::slice(uint begin0, uint end0, uint begin1, uint end1, Array2d<float> &array2d);
template void Array2d<bool>::slice(uint begin0, uint end0, uint begin1, uint end1, Array2d<bool> &array2d);

// ------------------------------------------------------------------------------------------------------------------ //
// 3D ARRAY                                                                                                           //
// ------------------------------------------------------------------------------------------------------------------ //

// Construct from shape
template <class T>
Array3d<T>::Array3d(uint size0, uint size1, uint size2)
{
    // Allocate axis 0
    mElements = new T**[size0];
    
    // Allocate axis 1
    for (uint ax0 = 0; ax0 < size0; ax0++)
    {
        mElements[ax0] = new T*[size1];
        
        // Allocate axis 2
        for (uint ax1 = 0; ax1 < size1; ax1++)
            mElements[ax0][ax1] = new T[size2];
    }
    
    // Total number of elements
    mSize = size0 * size1 * size2;
    
    // Shape of the array
    mShape.allocate(3);
    mShape[0] = size0;
    mShape[1] = size1;
    mShape[2] = size2;
}

template Array3d <uint> ::Array3d(uint size0, uint size1, uint size2);
template Array3d <float>::Array3d(uint size0, uint size1, uint size2);
template Array3d <bool> ::Array3d(uint size0, uint size1, uint size2);

// ------------------------------------------------------------------------------------------------------------------ //

// Constructor from shape and value
template <class T>
Array3d<T>::Array3d(uint size0, uint size1, uint size2, T value)
{
    // Allocate axis 0
    mElements = new T**[size0];
    
    // Allocate axis 1
    for (uint ax0 = 0; ax0 < size0; ax0++)
    {
        mElements[ax0] = new T*[size1];
        
        // Allocate axis 2 and initialize with value
        for (uint ax1 = 0; ax1 < size1; ax1++)
        {
            mElements[ax0][ax1] = new T[size2];
            for (uint ax2 = 0; ax2 < size2; ax2++)
                mElements[ax0][ax1][ax2] = value;
        }
    }
    
    // Total number of elements
    mSize = size0 * size1 * size2;
    
    // Shape of the array
    mShape.allocate(3);
    mShape[0] = size0;
    mShape[1] = size1;
    mShape[2] = size2;
}

template Array3d <uint> ::Array3d(uint size0, uint size1, uint size2, uint  value);
template Array3d <float>::Array3d(uint size0, uint size1, uint size2, float value);
template Array3d <bool> ::Array3d(uint size0, uint size1, uint size2, bool  value);

// ------------------------------------------------------------------------------------------------------------------ //

// Destructor
template <class T>
Array3d<T>::~Array3d()
{
    if (mElements)
    {
        for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        {
            if (mElements[ax0])
            {
                for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
                {
                    if (mElements[ax0][ax1])
                        delete[] mElements[ax0][ax1];
                }
                
                delete[] mElements[ax0];
            }
        }
        delete[] mElements;
    }
}

template Array3d <uint> ::~Array3d();
template Array3d <float>::~Array3d();
template Array3d <bool> ::~Array3d();

// Operators -------------------------------------------------------------------------------------------------------- //

// Copy assignment
template <class T>
void Array3d<T>::operator= (Array3d<T> &array3d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] = array3d[ax0][ax1][ax2];
}

template void Array3d <uint> ::operator= (Array3d <uint>  &array3d);
template void Array3d <float>::operator= (Array3d <float> &array3d);
template void Array3d <bool> ::operator= (Array3d <bool>  &array3d);

// ------------------------------------------------------------------------------------------------------------------ //

// Copy assignment
template <class T>
void Array3d<T>::operator= (T ***array3d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] = array3d[ax0][ax1][ax2];
}

template void Array3d <uint> ::operator= (uint  ***array3d);
template void Array3d <float>::operator= (float ***array3d);
template void Array3d <bool> ::operator= (bool  ***array3d);

// ------------------------------------------------------------------------------------------------------------------ //
// Subscript operator
template <class T>
T** Array3d<T>::operator[] (uint index)
{
    return mElements[index];
}

template uint**  Array3d <uint> ::operator[] (uint index);
template float** Array3d <float>::operator[] (uint index);
template bool**  Array3d <bool> ::operator[] (uint index);

// ------------------------------------------------------------------------------------------------------------------ //

// Return as constant
/*template <class T>
const T** Array3d<T>::operator[] (uint index) const
{
    return mElements[index];
}

template const uint**  Array3d <uint> ::operator[] (uint index) const;
template const float** Array3d <float>::operator[] (uint index) const;
template const bool**  Array3d <bool> ::operator[] (uint index) const;*/

// Inplace operators ---------------------------------------------------------------------------------------------- //

// Add by uint
template <class T>
void Array3d<T>::operator+= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] += value;
}

template void Array3d <uint> ::operator+= (uint value);
template void Array3d <float>::operator+= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract by uint
template <class T>
void Array3d<T>::operator-= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] -= value;
}

template void Array3d<float>::operator-= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by uint
template <class T>
void Array3d<T>::operator*= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] *= value;
}

template void Array3d<uint>::operator*= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by uint
template <class T>
void Array3d<T>::operator/= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] /= value;
}

template void Array3d <float>::operator/= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Add by int
template <class T>
void Array3d<T>::operator+= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] += value;
}

template void Array3d <uint> ::operator+= (int value);
template void Array3d <float>::operator+= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract by int
template <class T>
void Array3d<T>::operator-= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] -= value;
}

template void Array3d <float>::operator-= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by int
template <class T>
void Array3d<T>::operator*= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] *= value;
}

template void Array3d <uint> ::operator*= (int value);
template void Array3d <float>::operator*= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by int
template <class T>
void Array3d<T>::operator/= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] /= value;
}

template void Array3d <float>::operator/= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Add by float
template <class T>
void Array3d<T>::operator+= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] += value;
}

template void Array3d<float>::operator+= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract by float
template <class T>
void Array3d<T>::operator-= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] -= value;
}

template void Array3d<float>::operator-= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by float
template <class T>
void Array3d<T>::operator*= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] *= value;
}

template void Array3d<float>::operator*= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by float
template <class T>
void Array3d<T>::operator/= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] /= value;
}

template void Array3d<float>::operator/= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Add uint array
template <class T>
void Array3d<T>::operator+= (Array3d<uint> &array3d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] += array3d[ax0][ax1][ax2];
}

template void Array3d <uint> ::operator+= (Array3d<uint> &array3d);
template void Array3d <float>::operator+= (Array3d<uint> &array3d);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract uint array
template <class T>
void Array3d<T>::operator-= (Array3d<uint> &array3d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] -= array3d[ax0][ax1][ax2];
}

template void Array3d <float>::operator-= (Array3d<uint> &array3d);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply uint array
template <class T>
void Array3d<T>::operator*= (Array3d<uint> &array3d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] *= array3d[ax0][ax1][ax2];
}

template void Array3d <uint> ::operator*= (Array3d<uint> &array3d);
template void Array3d <float>::operator*= (Array3d<uint> &array3d);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide uint array
template <class T>
void Array3d<T>::operator/= (Array3d<uint> &array3d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] /= array3d[ax0][ax1][ax2];
}

template void Array3d<float>::operator/= (Array3d<uint> &array3d);

// ------------------------------------------------------------------------------------------------------------------ //

// Add float array
template <class T>
void Array3d<T>::operator+= (Array3d<float> &array3d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] += array3d[ax0][ax1][ax2];
}

template void Array3d<float>::operator+= (Array3d<float> &array3d);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract float array
template <class T>
void Array3d<T>::operator-= (Array3d<float> &array3d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] -= array3d[ax0][ax1][ax2];
}

template void Array3d<float>::operator-= (Array3d<float> &array3d);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by float array
template <class T>
void Array3d<T>::operator*= (Array3d<float> &array3d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] *= array3d[ax0][ax1][ax2];
}

template void Array3d<float>::operator*= (Array3d<float> &array3d);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by float array
template <class T>
void Array3d<T>::operator/= (Array3d<float> &array3d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] /= array3d[ax0][ax1][ax2];
}

template void Array3d<float>::operator/= (Array3d<float> &array3d);

// ------------------------------------------------------------------------------------------------------------------ //

// Return the total number of elements
template <class T>
const uint Array3d<T>::size() const
{
    return mSize;
}

template const uint Array3d <uint> ::size() const;
template const uint Array3d <float>::size() const;
template const uint Array3d <bool> ::size() const;

// ------------------------------------------------------------------------------------------------------------------ //

// Return the shape
template <class T>
const Array1d<uint> Array3d<T>::shape() const
{
    return mShape;
}

template const Array1d<uint> Array3d <uint> ::shape() const;
template const Array1d<uint> Array3d <float>::shape() const;
template const Array1d<uint> Array3d <bool> ::shape() const;

// ------------------------------------------------------------------------------------------------------------------ //

// Allocate memory (only once)
template <class T>
void Array3d<T>::allocate(Array1d<uint> shape)
{
    // Allocate only if it’s not yet allocated
    assert (!mElements);
    
    // Allocate axis 0
    mElements = new T**[shape[0]];
    
    // Allocate axis 1
    for (uint ax0 = 0; ax0 < shape[0]; ax0++)
    {
        mElements[ax0] = new T*[shape[1]];
        
        // Allocate axis 2
        for (uint ax1 = 0; ax1 < shape[1]; ax1++)
            mElements[ax0][ax1] = new T[shape[2]];
    }
    
    // Total number of the array
    mSize = shape[0] * shape [1] * shape[2];
    
    // Shape of the array
    mShape = shape;
}

template void Array3d <uint> ::allocate(Array1d <uint> shape);
template void Array3d <float>::allocate(Array1d <uint> shape);
template void Array3d <bool> ::allocate(Array1d <uint> shape);

// ------------------------------------------------------------------------------------------------------------------ //

// Allocate memory (only once)
template <class T>
void Array3d<T>::allocate(uint size0, uint size1, uint size2)
{
    // Allocate only if it’s not yet allocated
    assert (!mElements);
    
    // Allocate axis 0
    mElements = new T**[size0];
    
    // Allocate axis 1
    for (uint ax0 = 0; ax0 < size0; ax0++)
    {
        mElements[ax0] = new T*[size1];
        
        // Allocate axis 2
        for (uint ax1 = 0; ax1 < size1; ax1++)
            mElements[ax0][ax1] = new T[size2];
    }
    
    // Total number of the array
    mSize = size0 * size1 * size2;
    
    // Shape of the array
    mShape[0] = size0;
    mShape[1] = size1;
    mShape[2] = size2;
}

template void Array3d <uint> ::allocate(uint size0, uint size1, uint size2);
template void Array3d <float>::allocate(uint size0, uint size1, uint size2);
template void Array3d <bool> ::allocate(uint size0, uint size1, uint size2);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by uint value
template <class T>
void Array3d<T>::fill(uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] = value;
}

template void Array3d <uint> ::fill(uint value);
template void Array3d <float>::fill(uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by int value
template <class T>
void Array3d<T>::fill(int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] = value;
}

template void Array3d <uint> ::fill(int value);
template void Array3d <float>::fill(int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by float value
template <class T>
void Array3d<T>::fill(float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] = value;
}

template void Array3d <float>::fill(float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by boolean value
template <class T>
void Array3d<T>::fill(bool value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                mElements[ax0][ax1][ax2] = value;
}

template void Array3d<bool>::fill(bool value);

// ------------------------------------------------------------------------------------------------------------------ //

// Get the slice of the array
template <class T>
void Array3d<T>::slice(uint begin0, uint end0,
                       uint begin1, uint end1,
                       uint begin2, uint end2,
                       Array3d<T> &array3d)
{
    array3d.allocate(end0 - begin0 + 1, end1 - begin1 + 1, end2 - begin2 + 1);
    
    for (uint ax0 = begin0; ax0 < end0; ax0++)
        for (uint ax1 = begin1; ax1 < end1; ax1++)
            for (uint ax2 = begin2; ax2 < end2; ax2++)
                array3d[ax0 - begin0][ax1 - begin1][ax2 - begin2] = mElements[ax0][ax1][ax2];
}

template void Array3d <uint> ::slice(uint begin0, uint end0,
                                     uint begin1, uint end1,
                                     uint begin2, uint end2,
                                     Array3d <uint> &array3d);
template void Array3d <float>::slice(uint begin0, uint end0,
                                     uint begin1, uint end1,
                                     uint begin2, uint end2,
                                     Array3d <float> &array3d);
template void Array3d <bool> ::slice(uint begin0, uint end0,
                                     uint begin1, uint end1,
                                     uint begin2, uint end2,
                                     Array3d <bool> &array3d);

// ------------------------------------------------------------------------------------------------------------------ //
// 4D ARRAY                                                                                                           //
// ------------------------------------------------------------------------------------------------------------------ //

// ------------------------------------------------------------------------------------------------------------------ //

// Construct from shape
template <class T>
Array4d<T>::Array4d(uint size0, uint size1, uint size2, uint size3)
{
    // Allocate axis 0
    mElements = new T***[size0];
    
    // Allocate axis 1
    for (uint ax0 = 0; ax0 < size0; ax0++)
    {
        mElements[ax0] = new T**[size1];
        
        // Allocate axis 2
        for (uint ax1 = 0; ax1 < size1; ax1++)
        {
            mElements[ax0][ax1] = new T*[size2];
            
            // Allocate axis 3
            for (uint ax2 = 0; ax2 < size2; ax2++)
                mElements[ax0][ax1][ax2] = new T[size3];
        }
    }
    
    // Total number of elements
    mSize = size0 * size1 * size2 * size3;
    
    // Shape of the array
    mShape.allocate(4);
    mShape[0] = size0;
    mShape[1] = size1;
    mShape[2] = size2;
    mShape[3] = size3;
}

template Array4d <uint> ::Array4d(uint size0, uint size1, uint size2, uint size3);
template Array4d <float>::Array4d(uint size0, uint size1, uint size2, uint size3);
template Array4d <bool> ::Array4d(uint size0, uint size1, uint size2, uint size3);

// ------------------------------------------------------------------------------------------------------------------ //

// Constructor from shape and value
template <class T>
Array4d<T>::Array4d(uint size0, uint size1, uint size2, uint size3, T value)
{
    // Allocate axis 0
    mElements = new T***[size0];
    
    // Allocate axis 1
    for (uint ax0 = 0; ax0 < size0; ax0++)
    {
        mElements[ax0] = new T**[size1];
        
        // Allocate axis 2
        for (uint ax1 = 0; ax1 < size1; ax1++)
        {
            mElements[ax0][ax1] = new T*[size2];
            
            // Allocate axis 3 and initialize with value
            for (uint ax2 = 0; ax2 < size2; ax2++)
            {
                mElements[ax0][ax1][ax2] = new T[size3];
                for (uint ax3 = 0; ax3 < size3; ax3++)
                    mElements[ax0][ax1][ax2][ax3] = value;
                
            }
        }
    }
    
    // Total number of elements
    mSize = size0 * size1 * size2 * size3;
    
    // Shape of the array
    mShape.allocate(4);
    mShape[0] = size0;
    mShape[1] = size1;
    mShape[2] = size2;
    mShape[3] = size3;
}

template Array4d <uint> ::Array4d(uint size0, uint size1, uint size2, uint size3, uint  value);
template Array4d <float>::Array4d(uint size0, uint size1, uint size2, uint size3, float value);
template Array4d <bool> ::Array4d(uint size0, uint size1, uint size2, uint size3, bool  value);

// ------------------------------------------------------------------------------------------------------------------ //

// Destructor
template <class T>
Array4d<T>::~Array4d()
{
    if (mElements)
    {
        for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        {
            if (mElements[ax0])
            {
                for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
                {
                    if (mElements[ax0][ax1])
                    {
                        for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                        {
                            if (mElements[ax0][ax1][ax2])
                                delete[] mElements[ax0][ax1][ax2];
                        }
                        
                        delete[] mElements[ax0][ax1];
                    }
                }
                
                delete[] mElements[ax0];
            }
        }
        delete[] mElements;
    }
}

template Array4d <uint> ::~Array4d();
template Array4d <float>::~Array4d();
template Array4d <bool> ::~Array4d();

// Operators ---------------------------------------------------------------------------------------------------------- //

// Copy assignment
template <class T>
void Array4d<T>::operator= (Array4d<T> &array4d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] = array4d[ax0][ax1][ax2][ax3];
}

template void Array4d <uint> ::operator= (Array4d <uint>  &array4d);
template void Array4d <float>::operator= (Array4d <float> &array4d);
template void Array4d <bool> ::operator= (Array4d <bool>  &array4d);

// ------------------------------------------------------------------------------------------------------------------ //

// Subscript operator
template <class T>
T*** Array4d<T>::operator[] (uint index)
{
    return mElements[index];
}

template uint***  Array4d <uint> ::operator[] (uint index);
template float*** Array4d <float>::operator[] (uint index);
template bool***  Array4d <bool> ::operator[] (uint index);

// ------------------------------------------------------------------------------------------------------------------ //

/*template <class T>
const T*** Array4d<T>::operator[] (uint index) const
{
    return mElements[index];
}

template const uint***  Array4d <uint> ::operator[] (uint index) const;
template const float*** Array4d <float>::operator[] (uint index) const;
template const bool***  Array4d <bool> ::operator[] (uint index) const;*/


// Inplace operator ------------------------------------------------------------------------------------------------- //

// Add by uint
template <class T>
void Array4d<T>::operator+= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] += value;
}

template void Array4d <uint> ::operator+= (uint value);
template void Array4d <float>::operator+= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract by uint
template <class T>
void Array4d<T>::operator-= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] -= value;
}

template void Array4d <float>::operator-= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by uint
template <class T>
void Array4d<T>::operator*= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] *= value;
}

template void Array4d <uint> ::operator*= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by uint
template <class T>
void Array4d<T>::operator/= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] /= value;
}

template void Array4d <float>::operator/= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Add by int
template <class T>
void Array4d<T>::operator+= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] += value;
}

template void Array4d <uint> ::operator+= (int value);
template void Array4d <float>::operator+= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract by int
template <class T>
void Array4d<T>::operator-= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] -= value;
}

template void Array4d <float>::operator-= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by int
template <class T>
void Array4d<T>::operator*= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] *= value;
}

template void Array4d <uint> ::operator*= (int value);
template void Array4d <float>::operator*= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by int
template <class T>
void Array4d<T>::operator/= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] /= value;
}

template void Array4d <float>::operator/= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Add by float
template <class T>
void Array4d<T>::operator+= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] += value;
}

template void Array4d <float>::operator+= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract by float
template <class T>
void Array4d<T>::operator-= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] -= value;
}

template void Array4d <float>::operator-= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by float
template <class T>
void Array4d<T>::operator*= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] *= value;
}

template void Array4d <float>::operator*= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by float
template <class T>
void Array4d<T>::operator/= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] /= value;
}

template void Array4d <float>::operator/= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Add uint array
template <class T>
void Array4d<T>::operator+= (Array4d<uint> &array4d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] += array4d[ax0][ax1][ax2][ax3];
}

template void Array4d <uint> ::operator+= (Array4d<uint> &array4d);
template void Array4d <float>::operator+= (Array4d<uint> &array4d);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract uint array
template <class T>
void Array4d<T>::operator-= (Array4d<uint> &array4d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] -= array4d[ax0][ax1][ax2][ax3];
}

template void Array4d <float>::operator-= (Array4d<uint> &array4d);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply uint array
template <class T>
void Array4d<T>::operator*= (Array4d<uint> &array4d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] *= array4d[ax0][ax1][ax2][ax3];
}

template void Array4d <uint> ::operator*= (Array4d<uint> &array4d);
template void Array4d <float>::operator*= (Array4d<uint> &array4d);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide uint array
template <class T>
void Array4d<T>::operator/= (Array4d<uint> &array4d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] /= array4d[ax0][ax1][ax2][ax3];
}

template void Array4d <float>::operator/= (Array4d<uint> &array4d);

// ------------------------------------------------------------------------------------------------------------------ //

// Add float array
template <class T>
void Array4d<T>::operator+= (Array4d<float> &array4d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] += array4d[ax0][ax1][ax2][ax3];
}

template void Array4d <float>::operator+= (Array4d<float> &array4d);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract float array
template <class T>
void Array4d<T>::operator-= (Array4d<float> &array4d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] -= array4d[ax0][ax1][ax2][ax3];
}

template void Array4d <float>::operator-= (Array4d<float> &array4d);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by float array
template <class T>
void Array4d<T>::operator*= (Array4d<float> &array4d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] *= array4d[ax0][ax1][ax2][ax3];
}

template void Array4d <float>::operator*= (Array4d<float> &array4d);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by float array
template <class T>
void Array4d<T>::operator/= (Array4d<float> &array4d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] /= array4d[ax0][ax1][ax2][ax3];
}

template void Array4d <float>::operator/= (Array4d<float> &array4d);

// ------------------------------------------------------------------------------------------------------------------ //

// Return the total number of elements
template <class T>
const uint Array4d<T>::size() const
{
    return mSize;
}

template const uint Array4d <uint> ::size() const;
template const uint Array4d <float>::size() const;
template const uint Array4d <bool> ::size() const;

// ------------------------------------------------------------------------------------------------------------------ //

// Return the shape
template <class T>
const Array1d<uint> Array4d<T>::shape() const
{
    return mShape;
}

template const Array1d<uint> Array4d <uint> ::shape() const;
template const Array1d<uint> Array4d <float>::shape() const;
template const Array1d<uint> Array4d <bool> ::shape() const;

// ------------------------------------------------------------------------------------------------------------------ //

// Allocate memory (only once)
template <class T>
void Array4d<T>::allocate(Array1d<T> shape)
{
    // Allocate only if it’s not yet allocated
    assert (!mElements);
    
    // Allocate axis 0
    mElements = new T***[shape[0]];
    
    // Allocate axis 1
    for (uint ax0 = 0; ax0 < shape[0]; ax0++)
    {
        mElements[ax0] = new T**[shape[1]];
        
        // Allocate axis 2
        for (uint ax1 = 0; ax1 < shape[1]; ax1++)
        {
            mElements[ax0][ax1] = new T*[shape[2]];
            
            // Allocate axis 3
            for (uint ax2 = 0; ax2 < shape[2]; ax2++)
                mElements[ax0][ax1][ax2] = new T[shape[3]];
        }
    }
}

template void Array4d <uint> ::allocate(Array1d <uint>  shape);
template void Array4d <float>::allocate(Array1d <float> shape);
template void Array4d <bool> ::allocate(Array1d <bool>  shape);

// ------------------------------------------------------------------------------------------------------------------ //

// Allocate memory (only once)
template <class T>
void Array4d<T>::allocate(uint size0, uint size1, uint size2, uint size3)
{
    // Allocate only if it’s not yet allocated
    assert (!mElements);
    
    // Allocate axis 0
    mElements = new T***[size0];
    
    // Allocate axis 1
    for (uint ax0 = 0; ax0 < size0; ax0++)
    {
        mElements[ax0] = new T**[size1];
        
        // Allocate axis 2
        for (uint ax1 = 0; ax1 < size1; ax1++)
        {
            mElements[ax0][ax1] = new T*[size2];
            
            // Allocate axis 3
            for (uint ax2 = 0; ax2 < size2; ax2++)
                mElements[ax0][ax1][ax2] = new T[size3];
        }
    }
    
    // Total number of the array
    mSize = size0 * size1 * size2 * size3;
    
    // Shape of the array
    mShape[0] = size0;
    mShape[1] = size1;
    mShape[2] = size2;
    mShape[3] = size3;
}

template void Array4d <uint> ::allocate(uint size0, uint size1, uint size2, uint size3);
template void Array4d <float>::allocate(uint size0, uint size1, uint size2, uint size3);
template void Array4d <bool> ::allocate(uint size0, uint size1, uint size2, uint size3);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by uint value
template <class T>
void Array4d<T>::fill(uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] = value;
}

template void Array4d <uint> ::fill(uint value);
template void Array4d <float>::fill(uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by int value
template <class T>
void Array4d<T>::fill(int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] = value;
}

template void Array4d <uint> ::fill(int value);
template void Array4d <float>::fill(int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by float value
template <class T>
void Array4d<T>::fill(float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] = value;
}

template void Array4d <float>::fill(float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by boolean value
template <class T>
void Array4d<T>::fill(bool value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] = value;
}

template void Array4d <bool>::fill(bool value);

// ------------------------------------------------------------------------------------------------------------------ //

// Get the slice of the array
template <class T>
void Array4d<T>::slice(uint begin0, uint end0,
                       uint begin1, uint end1,
                       uint begin2, uint end2,
                       uint begin3, uint end3,
                       Array4d<T> &array4d)
{
    array4d.allocate(end0 - begin0 + 1, end1 - begin1 + 1, end2 - begin2 + 1, end3 - begin3 + 1);
    
    for (uint ax0 = begin0; ax0 < end0; ax0++)
        for (uint ax1 = begin1; ax1 < end1; ax1++)
            for (uint ax2 = begin2; ax2 < end2; ax2++)
                for (uint ax3 = begin3; ax3 < end3; ax3++)
                    array4d[ax0 - begin0][ax1 - begin1][ax2 - begin2][ax3 - begin3] = mElements[ax0][ax1][ax2][ax3];
}

template void Array4d  <uint>::slice(uint begin0, uint end0,
                                     uint begin1, uint end1,
                                     uint begin2, uint end2,
                                     uint begin3, uint end3,
                                     Array4d<uint> &array4d);
template void Array4d <float>::slice(uint begin0, uint end0,
                                     uint begin1, uint end1,
                                     uint begin2, uint end2,
                                     uint begin3, uint end3,
                                     Array4d<float> &array4d);
template void Array4d  <bool>::slice(uint begin0, uint end0,
                                     uint begin1, uint end1,
                                     uint begin2, uint end2,
                                     uint begin3, uint end3,
                                     Array4d<bool> &array4d);

// ------------------------------------------------------------------------------------------------------------------ //
// 5D ARRAY                                                                                                           //
// ------------------------------------------------------------------------------------------------------------------ //

// ------------------------------------------------------------------------------------------------------------------ //

// Construct from shape
template <class T>
Array5d<T>::Array5d(uint size0, uint size1, uint size2, uint size3, uint size4)
{
    // Allocate axis 0
    mElements = new T****[size0];
    
    // Allocate axis 1
    for (uint ax0 = 0; ax0 < size0; ax0++)
    {
        mElements[ax0] = new T***[size1];
        
        // Allocate axis 2
        for (uint ax1 = 0; ax1 < size1; ax1++)
        {
            mElements[ax0][ax1] = new T**[size2];
            
            // Allocate axis 3
            for (uint ax2 = 0; ax2 < size2; ax2++)
            {
                mElements[ax0][ax1][ax2] = new T*[size3];
                
                // Allocate axis 4
                for (uint ax3 = 0; ax3 < size3; ax3++)
                    mElements[ax0][ax1][ax2][ax3] = new T[size3];
            }
        }
    }
    
    // Total number of elements
    mSize = size0 * size1 * size2 * size3 * size4;
    
    // Shape of the array
    mShape.allocate(5);
    mShape[0] = size0;
    mShape[1] = size1;
    mShape[2] = size2;
    mShape[3] = size3;
    mShape[4] = size4;
}

template Array5d <uint> ::Array5d(uint size0, uint size1, uint size2, uint size3, uint size4);
template Array5d <float>::Array5d(uint size0, uint size1, uint size2, uint size3, uint size4);
template Array5d <bool> ::Array5d(uint size0, uint size1, uint size2, uint size3, uint size4);

// ------------------------------------------------------------------------------------------------------------------ //

// Constructor from shape and value
template <class T>
Array5d<T>::Array5d(uint size0, uint size1, uint size2, uint size3, uint size4, T value)
{
    // Allocate axis 0
    mElements = new T****[size0];
    
    // Allocate axis 1
    for (uint ax0 = 0; ax0 < size0; ax0++)
    {
        mElements[ax0] = new T***[size1];
        
        // Allocate axis 2
        for (uint ax1 = 0; ax1 < size1; ax1++)
        {
            mElements[ax0][ax1] = new T**[size2];
            
            // Allocate axis 3
            for (uint ax2 = 0; ax2 < size2; ax2++)
            {
                mElements[ax0][ax1][ax2] = new T*[size3];
                
                // Allocate axis 4 and initialize with value
                for (uint ax3 = 0; ax3 < size3; ax3++)
                {
                    mElements[ax0][ax1][ax2][ax3] = new T[size4];
                    for (uint ax4 = 0; ax4 < size4; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] = value;
                }
            }
        }
    }
    
    // Total number of elements
    mSize = size0 * size1 * size2 * size3 * size4;
    
    // Shape of the array
    mShape.allocate(5);
    mShape[0] = size0;
    mShape[1] = size1;
    mShape[2] = size2;
    mShape[3] = size3;
    mShape[4] = size4;
}

template Array5d <uint> ::Array5d(uint size0, uint size1, uint size2, uint size3, uint size4, uint  value);
template Array5d <float>::Array5d(uint size0, uint size1, uint size2, uint size3, uint size4, float value);
template Array5d <bool> ::Array5d(uint size0, uint size1, uint size2, uint size3, uint size4, bool  value);

// ------------------------------------------------------------------------------------------------------------------ //

// Destructor
template <class T>
Array5d<T>::~Array5d()
{
    if (mElements)
    {
        for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        {
            if (mElements[ax0])
            {
                for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
                {
                    if (mElements[ax0][ax1])
                    {
                        for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                        {
                            if (mElements[ax0][ax1][ax2])
                            {
                                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                                {
                                    if (mElements[ax0][ax1][ax2][ax3])
                                        delete[] mElements[ax0][ax1][ax2][ax3];
                                }
                                
                                delete[] mElements[ax0][ax1][ax2];
                            }
                        }
                        
                        delete[] mElements[ax0][ax1];
                    }
                }
                
                delete[] mElements[ax0];
            }
        }
        delete[] mElements;
    }
}

template Array5d <uint> ::~Array5d();
template Array5d <float>::~Array5d();
template Array5d <bool> ::~Array5d();

// Operators ---------------------------------------------------------------------------------------------------------- //

// Copy assignment
template <class T>
void Array5d<T>::operator= (Array5d<T> &array5d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] = array5d[ax0][ax1][ax2][ax3][ax4];
}

template void Array5d <uint> ::operator= (Array5d <uint>  &array5d);
template void Array5d <float>::operator= (Array5d <float> &array5d);
template void Array5d <bool> ::operator= (Array5d <bool>  &array5d);

// ------------------------------------------------------------------------------------------------------------------ //

// Subscript operator
template <class T>
T**** Array5d<T>::operator[] (uint index)
{
    return mElements[index];
}

template uint****  Array5d <uint> ::operator[] (uint index);
template float**** Array5d <float>::operator[] (uint index);
template bool****  Array5d <bool> ::operator[] (uint index);

// ------------------------------------------------------------------------------------------------------------------ //

/*//template <class T>
 const T**** Array5d<T>::operator[] (uint index) const
 {
 return mElements[index];
 }
 
 template const uint****  Array4d <uint> ::operator[] (uint index) const;
 template const float**** Array4d <float>::operator[] (uint index) const;
 template const bool****  Array4d <bool> ::operator[] (uint index) const;*/


// Inplace operator ------------------------------------------------------------------------------------------------- //

// Add by uint
template <class T>
void Array5d<T>::operator+= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] += value;
}

template void Array5d <uint> ::operator+= (uint value);
template void Array5d <float>::operator+= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract by uint
template <class T>
void Array5d<T>::operator-= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] -= value;
}

template void Array5d <float>::operator-= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by uint
template <class T>
void Array5d<T>::operator*= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] *= value;
}

template void Array5d <uint> ::operator*= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by uint
template <class T>
void Array5d<T>::operator/= (uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] /= value;
}

template void Array5d <float>::operator/= (uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Add by int
template <class T>
void Array5d<T>::operator+= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] += value;
}

template void Array5d <uint> ::operator+= (int value);
template void Array5d <float>::operator+= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract by int
template <class T>
void Array5d<T>::operator-= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] -= value;
}

template void Array5d <float>::operator-= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by int
template <class T>
void Array5d<T>::operator*= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] *= value;
}

template void Array5d <uint> ::operator*= (int value);
template void Array5d <float>::operator*= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by int
template <class T>
void Array5d<T>::operator/= (int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] /= value;
}

template void Array5d <float>::operator/= (int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Add by float
template <class T>
void Array5d<T>::operator+= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] += value;
}

template void Array5d <float>::operator+= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract by float
template <class T>
void Array5d<T>::operator-= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] -= value;
}

template void Array5d <float>::operator-= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by float
template <class T>
void Array5d<T>::operator*= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] *= value;
}

template void Array5d <float>::operator*= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by float
template <class T>
void Array5d<T>::operator/= (float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] /= value;
}

template void Array5d <float>::operator/= (float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Add uint array
template <class T>
void Array5d<T>::operator+= (Array5d<uint> &array5d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] += array5d[ax0][ax1][ax2][ax3][ax4];
}

template void Array5d <uint> ::operator+= (Array5d<uint> &array5d);
template void Array5d <float>::operator+= (Array5d<uint> &array5d);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract uint array
template <class T>
void Array5d<T>::operator-= (Array5d<uint> &array5d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] -= array5d[ax0][ax1][ax2][ax3][ax4];
}

template void Array5d <float>::operator-= (Array5d<uint> &array5d);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply uint array
template <class T>
void Array5d<T>::operator*= (Array5d<uint> &array5d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] *= array5d[ax0][ax1][ax2][ax3][ax4];
}

template void Array5d <uint> ::operator*= (Array5d<uint> &array5d);
template void Array5d <float>::operator*= (Array5d<uint> &array5d);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide uint array
template <class T>
void Array5d<T>::operator/= (Array5d<uint> &array5d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] /= array5d[ax0][ax1][ax2][ax3][ax4];
}

template void Array5d <float>::operator/= (Array5d<uint> &array5d);

// ------------------------------------------------------------------------------------------------------------------ //

// Add float array
template <class T>
void Array5d<T>::operator+= (Array5d<float> &array5d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] += array5d[ax0][ax1][ax2][ax3][ax4];
}

template void Array5d <float>::operator+= (Array5d<float> &array5d);

// ------------------------------------------------------------------------------------------------------------------ //

// Subtract float array
template <class T>
void Array5d<T>::operator-= (Array5d<float> &array5d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] -= array5d[ax0][ax1][ax2][ax3][ax4];
}

template void Array5d <float>::operator-= (Array5d<float> &array5d);

// ------------------------------------------------------------------------------------------------------------------ //

// Multiply by float array
template <class T>
void Array5d<T>::operator*= (Array5d<float> &array5d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] *= array5d[ax0][ax1][ax2][ax3][ax4];
}

template void Array5d <float>::operator*= (Array5d<float> &array5d);

// ------------------------------------------------------------------------------------------------------------------ //

// Divide by float array
template <class T>
void Array5d<T>::operator/= (Array5d<float> &array5d)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] /= array5d[ax0][ax1][ax2][ax3][ax4];
}

template void Array5d <float>::operator/= (Array5d<float> &array5d);

// ------------------------------------------------------------------------------------------------------------------ //

// Return the total number of elements
template <class T>
const uint Array5d<T>::size() const
{
    return mSize;
}

template const uint Array5d <uint> ::size() const;
template const uint Array5d <float>::size() const;
template const uint Array5d <bool> ::size() const;

// ------------------------------------------------------------------------------------------------------------------ //

// Return the shape
template <class T>
const Array1d<uint> Array5d<T>::shape() const
{
    return mShape;
}

template const Array1d<uint> Array5d <uint> ::shape() const;
template const Array1d<uint> Array5d <float>::shape() const;
template const Array1d<uint> Array5d <bool> ::shape() const;

// ------------------------------------------------------------------------------------------------------------------ //

// Allocate memory (only once)
/*template <class T>
void Array5d<T>::allocate(Array1d<T> shape)
{
    // Allocate only if it’s not yet allocated
    assert (!mElements);
    
    // Allocate axis 0
    mElements = new T****[shape[0]];
    
    // Allocate axis 1
    for (uint ax0 = 0; ax0 < shape[0]; ax0++)
    {
        mElements[ax0] = new T***[shape[1]];
        
        // Allocate axis 2
        for (uint ax1 = 0; ax1 < shape[1]; ax1++)
        {
            mElements[ax0][ax1] = new T**[shape[2]];
            
            // Allocate axis 3
            for (uint ax2 = 0; ax2 < shape[2]; ax2++)
            {
                mElements[ax0][ax1][ax2] = new T*[shape[3]];
                
                // Allocate axis 4
                for (uint ax3 = 0; ax3 < shape[3]; ax3++)
                    mElements[ax0][ax1][ax2][ax3] = new T[shape[4]];
            }
        }
    }
}

template void Array5d <uint> ::allocate(Array1d <uint>  shape);
template void Array5d <float>::allocate(Array1d <float> shape);
template void Array5d <bool> ::allocate(Array1d <bool>  shape);*/

// ------------------------------------------------------------------------------------------------------------------ //

// Allocate memory (only once)
template <class T>
void Array5d<T>::allocate(uint size0, uint size1, uint size2, uint size3, uint size4)
{
    // Allocate only if it’s not yet allocated
    assert (!mElements);
    
    // Allocate axis 0
    mElements = new T****[size0];
    
    // Allocate axis 1
    for (uint ax0 = 0; ax0 < size0; ax0++)
    {
        mElements[ax0] = new T***[size1];
        
        // Allocate axis 2
        for (uint ax1 = 0; ax1 < size1; ax1++)
        {
            mElements[ax0][ax1] = new T**[size2];
            
            // Allocate axis 3
            for (uint ax2 = 0; ax2 < size2; ax2++)
            {
                mElements[ax0][ax1][ax2] = new T*[size3];
                
                // Allocate axis 4
                for (uint ax3 = 0; ax3 < size3; ax3++)
                    mElements[ax0][ax1][ax2][ax3] = new T[size4];
            }
        }
    }
    
    // Total number of the array
    mSize = size0 * size1 * size2 * size3;
    
    // Shape of the array
    mShape[0] = size0;
    mShape[1] = size1;
    mShape[2] = size2;
    mShape[3] = size3;
    mShape[4] = size4;
}

template void Array5d <uint> ::allocate(uint size0, uint size1, uint size2, uint size3, uint size4);
template void Array5d <float>::allocate(uint size0, uint size1, uint size2, uint size3, uint size4);
template void Array5d <bool> ::allocate(uint size0, uint size1, uint size2, uint size3, uint size4);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by uint value
template <class T>
void Array5d<T>::fill(uint value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] = value;
}

template void Array5d <uint> ::fill(uint value);
template void Array5d <float>::fill(uint value);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by int value
template <class T>
void Array5d<T>::fill(int value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] = value;
}

template void Array5d <uint> ::fill(int value);
template void Array5d <float>::fill(int value);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by float value
template <class T>
void Array5d<T>::fill(float value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] = value;
}

template void Array5d <float>::fill(float value);

// ------------------------------------------------------------------------------------------------------------------ //

// Fill by boolean value
template <class T>
void Array5d<T>::fill(bool value)
{
    for (uint ax0 = 0; ax0 < mShape[0]; ax0++)
        for (uint ax1 = 0; ax1 < mShape[1]; ax1++)
            for (uint ax2 = 0; ax2 < mShape[2]; ax2++)
                for (uint ax3 = 0; ax3 < mShape[3]; ax3++)
                    for (uint ax4 = 0; ax4 < mShape[4]; ax4++)
                        mElements[ax0][ax1][ax2][ax3][ax4] = value;
}

template void Array5d <bool>::fill(bool value);

// ------------------------------------------------------------------------------------------------------------------ //

// Get the slice of the array
template <class T>
void Array5d<T>::slice(uint begin0, uint end0,
                       uint begin1, uint end1,
                       uint begin2, uint end2,
                       uint begin3, uint end3,
                       uint begin4, uint end4,
                       Array5d<T> &array5d)
{
    // Make sure the shape is right
    array5d.allocate(end0 - begin0 + 1, end1 - begin1 + 1, end2 - begin2 + 1, end3 - begin3 + 1, end4 - begin4 + 1);
    
    for (uint ax0 = begin0; ax0 < end0; ax0++)
        for (uint ax1 = begin1; ax1 < end1; ax1++)
            for (uint ax2 = begin2; ax2 < end2; ax2++)
                for (uint ax3 = begin3; ax3 < end3; ax3++)
                    for (uint ax4 = begin4; ax4 < end4; ax4++)
                        array5d[ax0 - begin0]
                               [ax1 - begin1]
                               [ax2 - begin2]
                               [ax3 - begin3]
                               [ax4 - begin4] = mElements[ax0][ax1][ax2][ax3][ax4];
}

template void Array5d  <uint>::slice(uint begin0, uint end0,
                                     uint begin1, uint end1,
                                     uint begin2, uint end2,
                                     uint begin3, uint end3,
                                     uint begin4, uint end4,
                                     Array5d<uint> &array5d);
template void Array5d <float>::slice(uint begin0, uint end0,
                                     uint begin1, uint end1,
                                     uint begin2, uint end2,
                                     uint begin3, uint end3,
                                     uint begin4, uint end4,
                                     Array5d<float> &array5d);
template void Array5d  <bool>::slice(uint begin0, uint end0,
                                     uint begin1, uint end1,
                                     uint begin2, uint end2,
                                     uint begin3, uint end3,
                                     uint begin4, uint end4,
                                     Array5d<bool> &array5d);

