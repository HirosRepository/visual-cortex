

#include "../include/ui/gui.hpp"
#include "../include/ui/weight-visualizer.hpp"
#include "../include/arrays.hpp"


#include <GLFW/glfw3.h>

#include <iostream>

int main(int argc, const char *argv[])
{
    // Main GUI object
    Gui gui((char*)"Weights"); //FIXME: conversion from string to char
    
    WeightVisualizer weightVisualizer;
    weightVisualizer.init(gui, "../shaders/squareShader.vert", "../shaders/squareShader.frag");
    
    Array5d<float> arr(2, 2, 2, 2, 1);
    arr[0][0][1][1][0] = 0.5f;
    arr[1][0][0][1][0] = 0.8f;
    
    // Main Loop
    while (!glfwWindowShouldClose(gui.getWindow()))
    {
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        glfwPollEvents();
        weightVisualizer.draw(arr);
        
        
        glfwSwapBuffers(gui.getWindow());
    }
    
    // Delete All Resources
    
    // Terminate
    glfwTerminate();
    
    return 0;

}
