

This is an old (abandoned) project which models human V1 cortex using spking neural network. The purpose is to demostrate (rudimentary) knowledge of OpenCV and OpenGL in C++.

The project consists of STDP-based learning algorithm and a display mechanism.

The learning algorithm is loosely based on the paper:

Kheradpisheh, S. R., Ganjtabesh, M., Thorpe, S. J., & Masquelier, T. (2018). STDP-based spiking deep convolutional neural networks for object recognition. Neural Networks, 99, 56-67.

Similar to the paper, the model learns Gabor-like features in an unsupervised fashion.

![image info](./v1-learned-patterns.png)
