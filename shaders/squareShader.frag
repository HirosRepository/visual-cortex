#version 330 core

//in vec2 TexCoords;
//out vec4 color;
out vec4 FragColor;

//uniform sampler2D image;
uniform float brightness;

void main()
{
    //color = vec4(1.0f, 1.0f, 1.0f, 0.0f);//vec4(spriteColor, 1.0) * texture(image, TexCoords);
    FragColor = vec4(1.0f, 1.0f, 1.0f, 0.0f) * brightness;
}
